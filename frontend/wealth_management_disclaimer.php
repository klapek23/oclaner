<?php require_once(dirname(__FILE__) . '/header.php'); ?>

    <section class="main-banner" style="background-image: url('/files/banner_wealth_management.jpg');">
        <div class="content">
            <article class="rounded">
                <h3>OCLANER WEALTH MANAGEMENT</h3>
                <h2>Create your <br>
                    growth story</h2>
                <p>Discover Oclaner Funds</p>
            </article>
        </div>
    </section>

    <section class="content-section standard-content dark-bg no-padding-bottom">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Asset Management</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>As a Singapore-based and Capital Markets-licensed Asset Management firm regulated by the Monetary Authority of Singapore, our investment management arm offers comprehensive and independent solutions to both individual and institutional clients as well as bespoke portfolio constructions for individuals, families, institutions and corporations.</p>
                    </div>
                </div>
            </div>
        </article>
    </section>

    <section class="content-section content-tabs-module page-tabs-module">
        <nav class="tabs-nav pager">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                            <li><span>Oclaner Mandates</span></li>
                            <li><span>Oclaner Funds UCITS IV SICAV</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="tabs-content slider-content">
            <section class="slide">
                <div class="content-section standard-content no-padding-bottom"">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Create your growth story</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>We offer individual and institutional clients four distinct UCITS IV compliant funds with four distinct strategies for different risk appetites. They have been developed, tested and fine-tuned to respond to specific needs be it gaining exposure to the fastest growing region in the world or to the top global companies. Regardless of the security, geography or industry we look at, understanding and minimising risk is our single most important preoccupation. All four funds are managed with our Oclaner signature: an active, flexible and conservative approach to investing.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="content-section accordion-module simply-accordion-module no-padding-top" >
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="accordion-content">
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Asia Pacific Equity Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                        </div>
                                        <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Provident World Equity Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                        <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Asian Bond Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Patrimonial Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="slide">
                <div class="content-section standard-content no-padding-bottom"">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Create your growth story</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>We offer individual and institutional clients four distinct UCITS IV compliant funds with four distinct strategies for different risk appetites. They have been developed, tested and fine-tuned to respond to specific needs be it gaining exposure to the fastest growing region in the world or to the top global companies. Regardless of the security, geography or industry we look at, understanding and minimising risk is our single most important preoccupation. All four funds are managed with our Oclaner signature: an active, flexible and conservative approach to investing.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="content-section accordion-module simply-accordion-module no-padding-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="accordion-content">
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Asia Pacific Equity Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                        </div>
                                        <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Provident World Equity Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Asian Bond Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Patrimonial Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>

    <section class="content-section commentary-section dark-bg" ng-app="commentaryApp" ng-controller="appCtrl">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="commentary-nav" ng-controller="navCtrl">
                        <ul>
                            <li ng-repeat="item in categories" ng-class="{'active' : activeCategoryId == item.id }"><span data-name="{{item.id}}" ng-click="changeOnClick($event)">{{item.name}}</span></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="commentary-content">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="filters accordion-module simply-accordion-module" ng-controller="filtersCtrl">
                                    <div class="accordion-content">
                                        <h3>Filters</h3>
                                        <section class="item" ng-repeat="filter in filters">
                                            <div class="inside">
                                                <span class="name">{{ filter.title }}</span>
                                                <article>
                                                    <ul>
                                                        <li ng-click="setActiveFilter(filter.name, '')"><span>All</span></li>
                                                        <li ng-repeat="item in filter.items | orderBy: item : true" ng-class="{'active' : activeFilters[filter.name] == item }"><span ng-click="filterByElement($event, filter.name)" data-filter-value="{{item}}">{{item}}</span></li>
                                                    </ul>
                                                </article>
                                            </div>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <section class="commentary-list" ng-controller="listCtrl">
                                    <article class="item" ng-repeat="item in items | toArray | filter: { year: activeFilters.years, subcategory: activeFilters.subcategories } | orderBy: year : true">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <h4>{{item.name}}</h4>
                                                <p>{{item.description}}</p>
                                                <em>{{item.subcategory}}</em>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="page-data">
                                                    <ul>
                                                        <li><a href="{{item.file}}" title="">
                                                                <i class="icon icon_loop-icon"></i>
                                                                <em>View</em>
                                                            </a></li>
                                                        <li><a href="{{item.file}}" title="">
                                                                <i class="icon icon_download-icon"></i>
                                                                <em>Download</em>
                                                            </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="disclaimer">
        <div class="table">
            <div class="cell">
                <div class="container">
                    <div class="col-md-12">
                        <section class="disclaimer-inside nano">
                            <article class="nano-content">
                                <h2>Oclamon Funds Disclaimer</h2>
                                <section>
                                    <p><strong>By accepting the terms and conditions set out below, you acknowledge and confirm that you are a qualified client:</strong></p>
                                    <ul>
                                        <li>Individual with a net personal assets exceed S$2,000,000 in value (or its equivalent in a foreign currency)</li>
                                        <li>A corporation with net assets exceeding S$10,000,000 in value (or its equivalent in a foreign currency)</li>
                                        <li>A corporation the sole business of which is to hold investments and the entire share capital of which is owned by one or more individuals, each of whom is an accredited investor</li>
                                        <li>A trustee of a trust the sole purpose of which is to hold investments and each beneficiary of which is an individual who is an accredited investor</li>
                                        <li>Institutional Investors</li>
                                        <li>Executive Officer, director or other general partner of the issuer</li>
                                        <li>Financial Institution</li>
                                        <li>Finance company</li>
                                        <li>A Statutory Body</li>
                                        <li>A Pension Fund or collective investment scheme</li>
                                        <li>Investment company</li>
                                        <li>Insurance company</li>
                                        <li>Broker or dealer</li>
                                        <li>Business development company</li>
                                        <li>Employee benefit plan or</li>
                                        <li>An entity in which the equity owners are accredited investors.</li>
                                    </ul>
                                    <p><strong>Terms:</strong><br>
                                        Please read the terms below before proceeding. The terms explain certain restrictions imposed by law on distribution of this information. In using this website, you agree to the below terms. If you do not agree to them you should exit this website now.</p>
                                    <p>This website provides information about Oclamon Real Estate Fund 1 (“OREF 1”), incorporated as an exempted company with limited liability under the laws of the Cayman Islands and is managed by Oclaner Asset Management Pte. Ltd. (“Oclaner AM”).</p>
                                    <p>The issuer of this website is Oclaner AM and the information on this website does not constitute an offer to sell or solicitation of an offer to buy an interest in the OREF 1, nor shall it form the basis of or be relied upon in connection with any contract or commitment whatsoever or be taken as investment advice. Any such offer or solicitation will be made only by means of delivery of a confidential private offering memorandum and relevant supplements (together, the “PPM”) relating to the OREF 1 to selected prospective qualified investors having professional experience in matters relating to investments which do not require immediate liquidity, and who are located in those territories where such offer or invitation could lawfully be made without compliance with any registration or any other legal requirement.</p>
                                    <p>The material contained in this website is intended solely for the person(s) accepting this disclaimer and the material on this site may not be transmitted (in any form) to any other person without Oclaner AM’s consent. This website generally may only be accessed by existing clients of Oclaner AM and prospective clients of Oclaner AM who are qualified investors.</p>
                                    <p><strong>No warranty or reliance</strong><br>
                                        In association with the merits of the proposed investment, investors should be aware of the risk factors involved in the investment. Investors may not get back the amount they originally invested. Investors should seek independent financial advice before making a commitment to invest. Investors should read the PPM of OREF 1 for further details including the risk factors before deciding whether to subscribe into OREF 1.</p>
                                    <p>Although information in this website may be based from sources deemed to be reliable, Oclaner AM does not guarantee the accuracy, adequacy or completeness of the information appearing on this website and expressly disclaims liability for any errors or omissions or from the results obtained from the use of such information. Opinions included in this website constitute the judgement of Oclaner AM at the time specified and may be subject to change without notice, they are not to be relied upon as authoritative or taken in substitution for the exercise of judgement by any recipient and are not intended to provide the sole basis of evaluation of any strategy or instrument discussed herein. As such, any person acting upon or in reliance of these materials does so entirely at his or her own risk. No warranty whatsoever is given and no liability whatsoever is accepted by Oclaner AM for any loss, arising directly or indirectly, as a result of any action or omission made in reliance of any information, opinion or projection made in this website.</p>
                                    <p>Investors should not rely solely on the information on this website and should read the PPM of OREF 1 in its entirety and are solely responsible for their compliance with the law and regulations, and should receive legal, financial and tax advice before making a decision to invest in OREF 1.</p>
                                    <p><strong>For further information, please contact Oclaner A.M.</strong></p>
                                </section>

                                <div class="popup-footer">
                                    <h3>Acceptance of Terms:</h3>
                                    <div class="popup-nav">
                                        <a href="#" class="button accept-button" title="Accept">Accept</a>
                                        <a href="/" class="button" title="Home Page">Return to Home Page</a>
                                        <a href="#" class="button" title="Return to previous page" onclick="window.history.back(); return false;">Return to previous page</a>
                                    </div>
                                    <p><strong>By clicking on Accept, I acknowledge and agree to the terms and conditions above. I undertake to access the information contained in this website for my personal use and not for commercial use.</strong></p>
                                </div>
                            </article>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            var $disclaimer = $('.disclaimer'),
                disclaimerPopup = new popup();

            disclaimerPopup.enablePopup($disclaimer, $('.accept-button'));
            disclaimerPopup.showPopup();
        });
    </script>

<?php require_once(dirname(__FILE__) . '/footer.php'); ?>
