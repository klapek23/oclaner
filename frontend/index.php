<?php require_once(dirname(__FILE__) . '/header.php'); ?>
	
    <div class="main-slider">
        <div class="slides slider-content">
            <div class="slide" style="background-image: url('/files/slide01.jpg')">
                <div class="content">
                    <article class="rounded">
                        <h3>OCLANER WEALTH MANAGEMENT</h3>
                        <h2>Create your growth story</h2>
                        <p>A transparent, open architecture. Access to the best advisors covering legal, tax, insurance, estate planning and investment matters. </p>
                        <a href="#" class="button big">View</a>
                    </article>
                </div>
            </div>
            <div class="slide" style="background-image: url('/files/slide01.jpg')">
                <div class="content">
                    <article class="rounded">
                        <h3>OCLANER WEALTH MANAGEMENT</h3>
                        <h2>Create your growth story</h2>
                        <p>A transparent, open architecture. Access to the best advisors covering legal, tax, insurance, estate planning and investment matters. </p>
                        <a href="#" class="button big">View</a>
                    </article>
                </div>
            </div>
            <div class="slide" style="background-image: url('/files/slide01.jpg')">
                <div class="content">
                    <article class="rounded">
                        <h3>OCLANER WEALTH MANAGEMENT</h3>
                        <h2>Create your growth story</h2>
                        <p>A transparent, open architecture. Access to the best advisors covering legal, tax, insurance, estate planning and investment matters. </p>
                        <a href="#" class="button big">View</a>
                    </article>
                </div>
            </div>
        </div>
        <nav class="pager">
            <ul>
                <li><span><em></em></span></li>
                <li><span><em></em></span></li>
                <li><span><em></em></span></li>
            </ul>
        </nav>
    </div>
	
<?php require_once(dirname(__FILE__) . '/footer.php'); ?>