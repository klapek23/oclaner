<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Oclaner - asset management ltd</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php $env = (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'dev'); ?>

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="/css/build/<?php echo $env; ?>/style.css">
    <script type="text/javascript" src="/js/build/<?php echo $env; ?>/scripts.js"></script>

    <?php $newspage = ($_SERVER['PHP_SELF'] == '/news.php' ? true : false); ?>
    <?php if($newspage): ?>
        <base href="/news/">
    <?php endif; ?>
</head>

<?php $homepage = ($_SERVER['PHP_SELF'] == '/index.php' ? true : false); ?>
<body class="<?php echo ($homepage ? 'homepage' : ''); ?>">

<header id="main-header">
    <h1><a href="/" title=""><img src="/img/main-logo.png" class="img-responsive" alt="Oclaner - asset management ltd"></a></h1>
</header>

<div id="main-menu">
    <button type="button" class="hamburger" id="main-menu-toggle">
        <span></span>
        <span></span>
        <span></span>
    </button>

    <ul>
        <li><a href="/" title="">HOME</a></li>
        <li><a href="/about_us" title="">ABOUT US</a></li>
        <li><a href="/wealth_management" title="">Wealth Management</a></li>
        <li><a href="/family_office_advisory" title="">FAMILY OFFICE ADVISORY</a></li>
        <li><a href="/direct_investments" title="">DIRECT INVESTMENTS</a></li>
        <li><a href="/news" title="">NEWS</a></li>
        <li><a href="/contact_us" title="">CONTACT US</a></li>
    </ul>

    <article>
        <h3>Oclaner Brand</h3>
        <p>Oclaner Asset Management is a Singapore-based asset management firm and Capital Markets Services license holder (issued by Monetary Authority of Singapore) with a growing team of industry experts and international partners.</p>
    </article>
</div>