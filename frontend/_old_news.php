<?php require_once(dirname(__FILE__) . '/header.php'); ?>
	
    <div class="main-banner" style="background-image: url('/files/banner_news.jpg');">
        <div class="content">
            <article class="rounded">
                <h3>NEWS</h3>
                <h2>Updates from <br>
                    Oclaner.</h2>
                <p>with Oclaner Family Office </p>
            </article>
        </div>
    </div>

    <section class="content-section news-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h2>News / Press / Events</h2>
                </div>
                <div class="col-md-4">
                    <nav class="filter">
                        <span class="title">Year:</span>
                        <ul>
                            <li>2015</li>
                            <li>2014</li>
                            <li>2013</li>
                        </ul>
                    </nav>
                </div>
            </div>
            <section class="news-list">
                <a href="#" title="" class="item">
                    <article>
                        <div class="item-data">May 08-05, 2015, by Jeremy Grant</div>
                        <h3>Role of Asian Family Offices expands from a narrow base</h3>
                        <p>Given the scale of wealth in Asia – in value and numbers of rich families – it would seem reasonable to assume that family offices are entrenched in the region. Credit Suisse estimates that globally there are close to 34,000 “ultra-high-net-worth” individuals, which it defines as those with a net wealth of $100m or more. About a quarter of those are in Asia</p>
                        <span class="more">Read more...</span>
                    </article>
                    <hr>
                </a>
                <a href="#" title="" class="item">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="/files/news_image.jpg" alt="" class="img-responsive newsImg">
                        </div>
                        <div class="col-md-8">
                            <article>
                                <div class="item-data">May 08-05, 2015, by Jeremy Grant</div>
                                <h3>Role of Asian Family Offices expands from a narrow base</h3>
                                <p>Given the scale of wealth in Asia – in value and numbers of rich families – it would seem reasonable to assume that family offices are entrenched in the region. Credit Suisse estimates that globally there are close to 34,000 “ultra-high-net-worth” individuals, which it defines as those with a net wealth of $100m or more. About a quarter of those are in Asia</p>
                                <span class="more">Read more...</span>
                            </article>
                        </div>
                    </div>
                    <hr>
                </a>
                <a href="#" title="" class="item">
                    <article>
                        <div class="item-data">May 08-05, 2015, by Jeremy Grant</div>
                        <h3>Role of Asian Family Offices expands from a narrow base</h3>
                        <p>Given the scale of wealth in Asia – in value and numbers of rich families – it would seem reasonable to assume that family offices are entrenched in the region. Credit Suisse estimates that globally there are close to 34,000 “ultra-high-net-worth” individuals, which it defines as those with a net wealth of $100m or more. About a quarter of those are in Asia</p>
                        <span class="more">Read more...</span>
                    </article>
                    <hr>
                </a>
            </section>
        </div>
    </section>

<?php require_once(dirname(__FILE__) . '/footer.php'); ?>