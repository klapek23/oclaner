$(document).ready(function() {

    //main menu toggle
    $('#main-menu-toggle').on('click', function(e) {
        toggleMainMenu(e.currentTarget);
    });

    $(document).on( 'keydown', function (e) {
        if(e.keyCode == 27 && $('#main-menu').hasClass('active')) {
            toggleMainMenu($('#main-menu-toggle'));
        }
    });

    function toggleMainMenu(button) {
        $(button).toggleClass('active');
        $('#main-menu').toggleClass('active');
        $('body').toggleClass('blocked');
    };

    //sticky header
    $(window).scroll( function() {
        var offsetTop = $(window).scrollTop();
        if(offsetTop > 0) {
            $('#main-header').addClass('sticky');
            $('#main-menu').addClass('sticky');
        } else {
            $('#main-header').removeClass('sticky');
            $('#main-menu').removeClass('sticky');
        }
    });

    //init main slider
    if($('.main-slider').length > 0) {
        var mainSlider = new sliderClass('.main-slider', {autoHeight: false, slideToContent: false});
        mainSlider.init();
    }

    //banner-slider
    if($('.banner-slider').length > 0) {
        var bannerSlider = new sliderClass('.banner-slider', {autoHeight: true, slideToContent: false, touchEvents: false});
        bannerSlider.init();
    }

    //init tabs slider
    if($('.tabs-module').length > 0) {
        var tabsSlider = new sliderClass('.tabs-module', {autoHeight: true, slideToContent: false}, animateNavClass);
        tabsSlider.init();
    }

    //init cmmentary section slider
    /*if($('.commentary-section').length > 0) {
        var commentaryModule = new commentaryModuleClass('.commentary-section', sliderClass, accordionModule);
        commentaryModule.init();
    }*/

    //init content tabs slider
    if($('.content-tabs-module').length > 0) {
        var contentTabsSlider = new sliderClass('.content-tabs-module', {autoHeight: true, slideToContent: false});
        contentTabsSlider.init();

        if(bannerSlider != undefined) {
            contentTabsSlider.$el.on('change-slide', function (e, data) {
                var newSlide = data.newSlide;

                bannerSlider.goToSlide(newSlide);
console.log('test')
                $('body').animate({scrollTop: 0}, 500);
            });
        }
    }

    //accordion module
    if($('.accordion-module').length > 0) {
        var accordionModule = new accordionClass('.accordion-module');
        accordionModule.init();

        if(contentTabsSlider != undefined) {
            accordionModule.$el.on('item-toggle-stop', function() {
                contentTabsSlider.updateSliderHeight();
            });
        }
    }

    //$.scrollSpeed(100, 800);
});

$(window).load(function() {

    $('body').addClass('faded');


});