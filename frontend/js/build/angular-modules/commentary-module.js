// declare a module
var app = angular.module('commentaryApp', ['ngAnimate', 'angular-toArrayFilter']);

app.factory('itemsFactory', function($http, $q) {

    return {
        getItemsByCat: function(cat) {
            return $http.get('/files/all.json').then( function(response) {
                return response.data[cat];
            });
        },
        getYearsByCat: function(cat) {
            return $http.get('/files/all.json').then( function(response) {
                var years = [];
                angular.forEach(response.data[cat], function(el, key) {
                    if(years.indexOf(el.year) == -1) {
                        years.push(el.year);
                    }
                });

                return years;
            });
        },
        getSubcategoriesByCat: function(cat) {
            return $http.get('/files/all.json').then( function(response) {
                var subcategories = [];
                angular.forEach(response.data[cat], function(el, key) {
                    if(subcategories.indexOf(el.subcategory) == -1) {
                        subcategories.push(el.subcategory);
                    }
                });

                return subcategories;
            });
        }
    }
});


//main app controller
app.controller('appCtrl', ['$scope', 'itemsFactory', function($scope, itemsFactory) {
    $scope.categories = [
        {
            name: 'Monthly Commentary',
            id: 'monthly-commentary'
        },
        {
            name: 'Factsheet',
            id: 'factsheet'
        },
        {
            name: 'KIID',
            id: 'kiid'
        },
        {
            name: 'Prospectus',
            id: 'prospectus'
        }
    ];
    $scope.activeCategoryId = 'monthly-commentary';

    $scope.items = [];
    $scope.filters = {
        years: {
            name: 'years',
            title: 'YEARS',
            items: []
        },
        subcategories: {
            name: 'subcategories',
            title: 'Categories',
            items: []
        }
    };

    $scope.activeFilters = {
        years: '',
        subcategories: ''
    };

    //setters
    $scope.setActiveCategoryId = function(id) {
        $scope.activeCategoryId = id;
    };
    $scope.setItems = function(newItems) {
        $scope.items = newItems;
    };

    $scope.setFilterYears = function(newYears) {
        $scope.filters.years.items = newYears;
    };

    $scope.setFiltersSubcategories = function(subcategories) {
        $scope.filters.subcategories.items = subcategories;
    };

    $scope.setActiveFilter = function(filter, value) {
        $scope.activeFilters[filter] = value;
    };


    //get first category items
    itemsFactory.getItemsByCat($scope.categories[0].id).then( function(items) {
        $scope.items = items;
    });

    //get first category years filter
    itemsFactory.getYearsByCat($scope.categories[0].id).then( function(years) {
        $scope.filters.years.items = years;
    });

    //get subcategories filter
    itemsFactory.getSubcategoriesByCat($scope.categories[0].id).then( function(subcategories) {
        $scope.filters.subcategories.items = subcategories;
    });
}]);



app.controller('filtersCtrl', ['$scope', function($scope) {
    $scope.filterByYear = function(year) {
        $scope.setActiveFilter('year', year);
    };

    $scope.filterByElement = function($event, filter) {
        var value = $($event.currentTarget).text();
        console.log(value, filter);
        $scope.setActiveFilter(filter, value);
    };
}]);


app.controller('navCtrl', ['$scope', 'itemsFactory', function($scope, itemsFactory) {

    $scope.changeOnClick = function($event) {
        var cat = $($event.currentTarget).data('name');

        $scope.setActiveCategoryId(cat);

        itemsFactory.getItemsByCat(cat).then( function(items) {
            $scope.setItems(items);
        });

        itemsFactory.getYearsByCat(cat).then( function(years) {
            $scope.setFilterYears(years);
        });

        itemsFactory.getSubcategoriesByCat(cat).then( function(subcategories) {
            $scope.setFiltersSubcategories(subcategories);
        });

        $scope.setActiveFilter('years', '');
        $scope.setActiveFilter('subcategories', '');
    };
}]);


app.controller('listCtrl', ['$scope', '$http', function($scope, $http) {

}]);
//# sourceMappingURL=commentary-module.js.map