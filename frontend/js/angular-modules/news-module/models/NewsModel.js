//data
newsApp.factory('NewsModel', function($http) {

    return {
        getNews: function() {
            return $http.get('/files/news.json').then( function(response) {
                return response.data;
            });
        },
        getSingleNewsById: function(id) {
            return $http.get('/files/news.json').then( function(response) {
                return response.data;
            });
        },
        getSingleNewsByAlias: function(alias) {
            return $http.get('/files/news.json').then( function(response) {
                var data = response.data,
                    news = {};

                angular.forEach(data, function(obj, key) {
                    if(obj.alias == alias) {
                        news = obj;

                        return false;
                    }
                });

                return news;
            });
        },
        getYears: function() {
            return $http.get('/files/news.json').then( function(response) {
                var years = [];
                angular.forEach(response.data, function(el, key) {
                    var date = new Date(el.date),
                        year = date.getFullYear();

                    if(years.indexOf(year) == -1) {
                        years.push(year);
                    }
                });

                return years;
            });
        }
    }
});