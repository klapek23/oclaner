//main app controller
newsApp.controller('AppCtrl', ['$scope', 'NewsModel', function($scope, NewsModel, $animate) {
    $scope.appView = 'list';
    $scope.appClass = 'news-content';

    $scope.activeFilters = {
        year    :   ''
    };

    $scope.setAppView = function(view) {
          $scope.appView = view;
    };

    $scope.setAppClass = function(appClass) {
        $scope.appClass = appClass;
    };

    $scope.changeView = function(view, appClass) {
        $scope.setAppView(view);
        $scope.setAppClass(appClass);
    };

    $scope.back = function() {
        window.history.back();
    };

    $scope.setActiveFilters = function(filter, value) {
        $scope.activeFilters[filter] = value;
    }
}]);