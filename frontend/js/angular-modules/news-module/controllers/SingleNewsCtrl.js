//main app controller
newsApp.controller('SingleNewsCtrl', ['$scope', '$routeParams', '$sce', 'NewsModel', function($scope, $routeParams, $sce, NewsModel) {
    var alias = $routeParams.newsalias;

    $scope.setAppView('single');
    $scope.setAppClass('single-news-content');

    NewsModel.getSingleNewsByAlias(alias).then( function(newsObj) {
        $scope.news = newsObj;
        $scope.news.content = $sce.trustAsHtml($scope.news.content);
    });
}]);