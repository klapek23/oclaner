//main app controller
newsApp.controller('NewsListCtrl', ['$scope', 'NewsModel', function($scope, NewsModel) {
    $scope.setAppView('list');
    $scope.setAppClass('news-content');

    //get all news
    NewsModel.getNews().then( function(newsArray) {
        $scope.newsList = newsArray;
    });
}]);
