newsApp.directive("newsAnimate", ['$animate', function($animate){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            var $spinner = $(elem).siblings('.loader');

            $animate.on('enter', elem, function(element, phase) {

                //check element
                if($(element).hasClass('newsAnimate')) {
                    switch (phase) {
                        case 'start':
                            $spinner.addClass('active');
                            break;
                        case 'close':
                            $spinner.removeClass('active');
                            break;
                    };
                }
            });
        }
    };
}]);