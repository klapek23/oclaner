// declare a module
var newsApp = angular.module('newsApp', ['ngRoute', 'ngAnimate']);

newsApp.config(function ($routeProvider, $locationProvider) {
    var appRootPath     =   '/js/angular-modules/news-module/',
        controllersPath =   appRootPath + 'controllers/',
        modelssPath =   appRootPath + 'models/',
        templatesPath =   appRootPath + 'templates/';

    $routeProvider
        // set route for the dynamic page
        .when('/',
        {
            controller: 'NewsListCtrl',
            templateUrl: templatesPath + 'news-list.html'
        })
        .when('/:newsalias',
        {
            controller: 'SingleNewsCtrl',
            templateUrl: templatesPath + 'single-news.html'
        })
        .otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode(true).hashPrefix('!');
});