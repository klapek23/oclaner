var accordionClass = function(el) {
    this.$el = $(el);
    this.items = this.$el.find('.item');
};

accordionClass.prototype = {
    constructor: accordionClass,
    init: function() {
        $(this.items).find('.accordion-toggle').on('click', function(e) {
             var $button    = $(e.currentTarget),
                 $item      = $button.closest('.item');

             this.toggleItem($item);
        }.bind(this));

        this.$el.trigger('initialize');
    },
    toggleItem: function($item) {
        this.$el.trigger('item-toggle-start');

        $item.toggleClass('opened').find('article').slideToggle(500, 'swing', function() {
            this.$el.trigger('item-toggle-stop');
        }.bind(this));
    }
};