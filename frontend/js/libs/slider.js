var sliderClass = function(el, options, animateNavClass) {
    this.$el = $(el);
    this.slides = this.getSlides();
    this.$activeSlide = {};
    this.pager = {};
    this.options = {};

    if(animateNavClass) {
        this.animateMenu =  new animateNavClass('.tabs-module .tabs-nav') || false;
    }

    this.options.autoHeight = (typeof options.autoHeight === 'undefined') ? true : options.autoHeight;
    this.options.slideToContent  = (typeof options.slideToContent === 'undefined') ? true : options.slideToContent;
    this.options.touchEvents = (typeof options.touchEvents === 'undefined') ? true : options.touchEvents;
};

sliderClass.prototype = {
    constructor: sliderClass,
    init: function() {
        this.$activeSlide = this.slides[0].addClass('active');
        this.pager.$el = this.getPager();
        this.pager.$items = this.pager.$el.find('li');

        this.updateSlides();

        this.pager.$items.eq(0).addClass('active');

        this.pager.$items.on('click', function(e) {
            var newSlideIndex = $(e.currentTarget).index();
            this.goToSlide(newSlideIndex);
        }.bind(this));

        $(window).load( function() {
            if(this.options.autoHeight) {
                this.updateSliderHeight(this.$activeSlide.height());
            }
        }.bind(this));

        //window resize
        $(window).resize(function(){
            this.updateSliderHeight(this.$activeSlide.height());
        }.bind(this));

        //mobile events
        if(this.options.touchEvents) {
            this.$el.on('swipeleft', function (e) {
                if (this.$activeSlide.next('.slide').length > 0) {
                    this.goToSlide(this.$activeSlide.next().index());
                }
            }.bind(this));

            this.$el.on('swiperight', function (e) {
                if (this.$activeSlide.prev('.slide').length > 0) {
                    this.goToSlide(this.$activeSlide.prev().index());
                }
            }.bind(this));
        };

        if(this.animateMenu) {
            $(window).load( function() {
                this.animateMenu.init();
            }.bind(this));
        };

        this.$el.trigger('initialize');
    },
    getSlides: function() {
        var slidesArray = [];
        $.each(this.$el.find('.slide'), function(i,el) {
            slidesArray.push($(el));
        });

        return slidesArray;
    },
    goToSlide: function(newSlide) {
        if(newSlide != this.$activeSlide.index()) {
            this.$activeSlide.removeClass('active');
            this.$activeSlide = this.slides[newSlide].removeClass('prev-slide').removeClass('next-slide').addClass('active');

            this.pager.$el.find('.active').removeClass('active');
            this.pager.$items.eq(newSlide).addClass('active');

            this.updateSlides();

            if(this.options.autoHeight) {
                this.updateSliderHeight(this.$activeSlide.height());
            }

            if(this.options.slideToContent) {
                $('body').animate({scrollTop: (this.$el.offset().top - $('#main-header').height())}, 500);
            }

            var $activeMenuItem = this.pager.$items.eq(newSlide);
            if(this.animateMenu) {
                this.animateMenu.changeActive($activeMenuItem);
            }

            this.$el.trigger('change-slide', {newSlide: newSlide});
        }
    },
    getPager: function() {
        return this.$el.find('.pager');
    },
    updateSliderHeight: function(newHeight) {
        var newHeight = newHeight || this.$activeSlide.height();
        this.$el.find('.slider-content').css({height: newHeight});

        this.$el.trigger('update-height');
    },
    updateSlides: function() {
        $.each(this.slides, function(i,el) {
            $(el).removeClass('next-slide');
            $(el).removeClass('prev-slide');

            if(i > this.$activeSlide.index()) {
                $(el).addClass('next-slide');
            } else if(i < this.$activeSlide.index()) {
                $(el).addClass('prev-slide');
            }
        }.bind(this))
    }
};