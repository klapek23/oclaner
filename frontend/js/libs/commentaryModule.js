var commentaryModuleClass = function(el, sliderClass, accordionClass) {
    this.$el = $(el);
    this.slider = new sliderClass(el, {autoHeight: true, slideToContent: false});
    this.categories = {
        $nav: this.$el.find('.commentary-nav'),
        $items: this.$el.find('.commentary-nav li').children('span'),
        catsNames: (function() {
            var cats = [];
            $.each(this.$el.find('.commentary-nav li'), function(i, el) {
                cats.push($(el).children().data('name'));
            }.bind(this));

            return cats;
        }.bind(this)())
    };
    this.$list = this.$el.find('.commentary-list');
    this.itemHTML = '<article class="item">'
    + '<div class="row">'
        + '<div class="col-md-7">'
            + '<h4></h4>'
            + '<p></p>'
        + '</div>'
        + '<div class="col-md-5">'
            + '<div class="page-data">'
                + '<ul>'
                    + '<li>'
                       + '<a href="" title="" class="view-file">'
                        + '<object type="image/svg+xml" data="/files/loop-icon.svg"></object>'
                        + '<em></em>'
                    + '</a></li>'
                + '<li>'
                + '<a href="" title="" class="download-file">'
                    + '<object type="image/svg+xml" data="/files/download-icon.svg"></object>'
                    + '<em></em>'
                + '</a></li>'
            + '</ul>'
        + '</div>'
        + '</div>'
    + '</article>';
};

commentaryModuleClass.prototype = {
    init: function() {
        this.slider.init();

        this.slider.$el.on('change-slide', function(e, data) {
            var newIndex = data.newSlide;
            this.changeCategory(this.categories.catsNames[newIndex]);
        }.bind(this));
    },
    changeCategory: function(catName) {
        $.getJSON('/files/'+ catName +'.json', function(data, status) {
            this.updateList(data)
        }.bind(this));
    },
    updateList: function(newData) {
        this.$list.css({display: 'none'});
        this.$list.find('.item').remove();

        var years = [],
            posts = [];
        $.each(newData, function(name, data) {
            years.push(name);
            $.each(data, function(i, item) {
                posts.push(item);
            });
        });

        $.each(posts, function(i, el) {
            var $newItem = $(this.itemHTML).appendTo(this.$list);
            $newItem.find('h4').text(el.name);
            $newItem.find('p').text(el.description);
            $newItem.find('.view-file').attr('href', el.file);
            $newItem.find('.download-file').attr('href', el.file);
        }.bind(this));

        setTimeout(function() {
            this.$list.fadeIn(500, function() {
                this.slider.updateSliderHeight();
            }.bind(this));
        }.bind(this), 6000);
    }
};