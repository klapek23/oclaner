try {
  cookieInfo = {
    $container : null,
    accepted : function(){ return; },
    show : function(){
      var pt = {
        firefox: 'http://support.mozilla.org/pl/kb/W%C5%82%C4%85czanie%20i%20wy%C5%82%C4%85czanie%20obs%C5%82ugi%20ciasteczek',
        opera: 'http://help.opera.com/Linux/12.10/pl/cookies.html',
        chrome: 'http://support.google.com/chrome/bin/answer.py?hl=pl&answer=95647',
        safari: 'http://support.apple.com/kb/ph5042',
        msie: 'http://windows.microsoft.com/pl-pl/internet-explorer/change-ie-settings#ie=ie-'
        };
      var r = navigator.userAgent.match(/(opera|chrome|safari|firefox|msie)\/?\s*/i),
      r = r[1].toLowerCase(),
      pu = pt[r];
      
      var info_pl = 'W ramach naszej strony stosujemy pliki cookies w celu świadczenia usług na najwyższym poziomie, dzięki czemu dostosowuje się ona do Twoich indywidualnych potrzeb. Korzystanie z witryny bez zmiany ustawień cookies oznacza, że będą one zamieszczane w Twoim urządzeniu końcowym. Możesz w każdym czasie dokonać zmiany ustawień dotyczących cookies. Więcej informacji znajdziesz w naszej Polityce Prywatności.';
      
      var c = $.cookie('cookieInfo');
      if (c) return false;
      
      var $inner = $('<div class="content_block">').css({
        position: 'relative', 
        padding: '10px 40px 10px 20px'
      });
          
      $('<p class="span_full">')
        .css({
          padding:0,
          margin:0,
          color:'#1a2038',
          'font-size':'12px',
          'line-height':'18px'
        })
        .html(info_pl)
        .appendTo($inner);
        
      $('a', $inner).attr({target:'_blank'});        
        
      $('<a>')
        .css({
          position: 'absolute',
          top: 14,
          right: 14,
          width: 10,
          height: 10,
          padding: '4px',
          display: 'block',
          color: '#fff',
          background: '#148cc8',
          borderRadius: 9,
          textAlign: 'center',
          verticalAlign: 'middle',
          font:'bold 10px/10px "Open Sans",Arial,Helvetica,sans-serif',
          cursor: 'pointer'
        }).
        html('X')
        .click(cookieInfo.accept)
        .appendTo($inner);
      
      cookieInfo.$container = $('<div>')
        .attr('id','cookieInfo')
        .css({
          position: 'relative',
          zIndex: 99999,
          backgroundColor: '#e1e4e9',
          'border-bottom': '1px solid #96dcfa'
        })
        .append($inner)
        .click(function(e){ e.stopPropagation()} )
        .insertBefore('body>*:first-child');
          
      //$(document.body).click(cookieInfo.accept);
      
      return true;        
    },
    accept : function(){
      cookieInfo.$container.slideUp(400, function() {
        cookieInfo.$container.remove();
      });
      
      $.cookie('cookieInfo', '1', { expires: 365 });
    }
  };


  $(function($){
    cookieInfo.show();
  });

} catch(e){
  //console.log(e);
};


/*!
 * jQuery Cookie Plugin v1.3.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as anonymous module.
    define(['jquery'], factory);
  } else {
    // Browser globals.
    factory(jQuery);
  }
}(function ($) {

  var pluses = /\+/g;

  function raw(s) {
    return s;
  }

  function decoded(s) {
    return decodeURIComponent(s.replace(pluses, ' '));
  }

  function converted(s) {
    if (s.indexOf('"') === 0) {
      // This is a quoted cookie as according to RFC2068, unescape
      s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
    }
    try {
      return config.json ? JSON.parse(s) : s;
    } catch(er) {}
  }

  var config = $.cookie = function (key, value, options) {

    // write
    if (value !== undefined) {
      options = $.extend({}, config.defaults, options);

      if (typeof options.expires === 'number') {
        var days = options.expires, t = options.expires = new Date();
        t.setDate(t.getDate() + days);
      }

      value = config.json ? JSON.stringify(value) : String(value);

      return (document.cookie = [
        config.raw ? key : encodeURIComponent(key),
        '=',
        config.raw ? value : encodeURIComponent(value),
        options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
        options.path    ? '; path=' + options.path : '',
        options.domain  ? '; domain=' + options.domain : '',
        options.secure  ? '; secure' : ''
      ].join(''));
    }

    // read
    var decode = config.raw ? raw : decoded;
    var cookies = document.cookie.split('; ');
    var result = key ? undefined : {};
    for (var i = 0, l = cookies.length; i < l; i++) {
      var parts = cookies[i].split('=');
      var name = decode(parts.shift());
      var cookie = decode(parts.join('='));

      if (key && key === name) {
        result = converted(cookie);
        break;
      }

      if (!key) {
        result[name] = converted(cookie);
      }
    }

    return result;
  };

  config.defaults = {};

  $.removeCookie = function (key, options) {
    if ($.cookie(key) !== undefined) {
      // Must not alter options, thus extending a fresh object...
      $.cookie(key, '', $.extend({}, options, { expires: -1 }));
      return true;
    }
    return false;
  };

}));