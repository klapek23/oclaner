<?php require_once(dirname(__FILE__) . '/header.php'); ?>
	
    <div class="main-banner" style="background-image: url('/files/banner_direct_investments.jpg');">
        <div class="content">
            <article class="rounded">
                <h3>DIRECT INVESTMENTS</h3>
                <h2>Your reach, <br>
                    your bridge</h2>
                <p>Oclaner Direct Investments</p>
            </article>
        </div>
    </div>

    <section class="content-section standard-content">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Direct Investments</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>We connect people and businesses to off-market investments. Through our strong global network, we have enhanced our capabilities and expertise in maximising revenue generating opportunities. Whether it is assisting in the broadening of existing business lines or finding new ventures, our people-intensive, consultative due-diligence process ensures that you have an accurate risk-reward ratio of the project before making the big leap. Confidentiality and professionalism are the hallmarks of our work.
                        </p>
                    </div>
                </div>
            </div>
        </article>
    </section>

    <section class="content-section tabs-module border-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="tabs-nav pager">
                        <ul>
                            <li><span>Club Deals</span> <hr></li>
                            <li><span>Private Equity</span> <hr></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs-content slider-content">
                        <article class="slide"><p>Access to unique, off-market deals and co-investment opportunities in conjunction with some of our other clients worldwide.</p></article>
                        <article class="slide"><p>With a dedicated structure and distinctive model in place, we cover our clients specific needs by empowering them with control over the management and objectives of their family office as well as focusing on wealth preservation and trans-generational planning through Trust & Legal Coordination (Estate Planning), Financial Consolidation & Reporting, Investment Management and Bridging (Direct Investments).</p></article>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-section two-cols-module">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Oclamon Fund 1</h2>
                    <h3>Real estate opportunities</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <article class="left-column">
                        <p>Oclamon Fund 1 revolves around a unique real-estate investment opportunity at the gateway of Medini’s Business District. With a Gross Development Value (GDV) estimated at US$20 billion over the next 15 to 20 years, Medini Nusajaya is strategically located in the heart of Iskandar. Medini is poised to become the region’s main business centre and as such it has become a prime attraction for investors and the focal point of our fund.</p>
                    </article>
                </div>
                <div class="col-md-6 col-sm-6">
                    <article class="right-column">
                        <p>Through our strategic partnership with local experts, we have been able to embark on the iconic UMCITY Medini Lakeside development venture. A 1.1 million sq ft and 1 billion ringgit mixed use project, UMCITY Medini Lakeside is our partner UMLand’s flagship project in the region. It is one of the only lake fronting project in Medini and is designed to showcase innovation and sustainability suited to an international audience.</p>
                    </article>
                </div>
            </div>
        </div>
    </section>

    <section class="content-section content-tabs-module no-padding-top">
        <nav class="tabs-nav pager">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                            <li><span>About Medini</span></li>
                            <li><span>Our Strategy</span></li>
                            <li><span>About Our Partner</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs-content slider-content">
                        <section class="slide">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <img src="/files/direct_investments_tabs_image.jpg" alt="" class="img-responsive hg-img-full-width">
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <h4>The Rising Giant</h4>
                                    <article>
                                        <p>Benefiting from the Iskandar Regional Development Programme, surrounded by a habitat of lush greenery and sea, Medini offers a strategic location right in the core of Iskandar. Its proximity to Singapore (40 minutes away from the 4th largest financial centre in the world), its availability of world-class infrastructure, its connectivity in terms of transportation and network access are paramount to a highly pro-business environment.</p>

                                        <p>In the short span of its existence, steady growth has been seen in the high-tech manufacturing, bio-technology, creative, medical, education and tourism hubs. Private investments have taken over public investment as the main source of funding to develop the region. This is especially evident in Medini, where capital investments have been allocated to soft infrastructure projects such as healthcare (Gleneagles Hospital), education (Educity™), lifestyle (Legoland™), and creative (Pinewood Studios).</p>

                                        <p>Besides integrated infrastructures coupled with high-quality education, healthcare and other first-rate community facilities, Medini offers a high standard of living at a much lower cost in comparison with Singapore, Hong Kong, Taiwan, Japan and Korea. With the support of the Malaysian government, it offers development opportunities that are more cost-efficient than any other established cities in Asia.</p>
                                    </article>
                                </div>
                            </div>
                        </section>
                        <section class="slide">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <img src="/files/direct_investments_tabs_image.jpg" alt="" class="img-responsive hg-img-full-width">
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <h4>The Rising Giant</h4>
                                    <article>
                                        <p>Benefiting from the Iskandar Regional Development Programme, surrounded by a habitat of lush greenery and sea, Medini offers a strategic location right in the core of Iskandar. Its proximity to Singapore (40 minutes away from the 4th largest financial centre in the world), its availability of world-class infrastructure, its connectivity in terms of transportation and network access are paramount to a highly pro-business environment.</p>

                                        <p>In the short span of its existence, steady growth has been seen in the high-tech manufacturing, bio-technology, creative, medical, education and tourism hubs. Private investments have taken over public investment as the main source of funding to develop the region. This is especially evident in Medini, where capital investments have been allocated to soft infrastructure projects such as healthcare (Gleneagles Hospital), education (Educity™), lifestyle (Legoland™), and creative (Pinewood Studios).</p>

                                        <p>Besides integrated infrastructures coupled with high-quality education, healthcare and other first-rate community facilities, Medini offers a high standard of living at a much lower cost in comparison with Singapore, Hong Kong, Taiwan, Japan and Korea. With the support of the Malaysian government, it offers development opportunities that are more cost-efficient than any other established cities in Asia.</p>
                                    </article>
                                </div>
                            </div>
                        </section>
                        <section class="slide">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <img src="/files/direct_investments_tabs_image.jpg" alt="" class="img-responsive hg-img-full-width">
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <h4>The Rising Giant</h4>
                                    <article>
                                        <p>Benefiting from the Iskandar Regional Development Programme, surrounded by a habitat of lush greenery and sea, Medini offers a strategic location right in the core of Iskandar. Its proximity to Singapore (40 minutes away from the 4th largest financial centre in the world), its availability of world-class infrastructure, its connectivity in terms of transportation and network access are paramount to a highly pro-business environment.</p>

                                        <p>In the short span of its existence, steady growth has been seen in the high-tech manufacturing, bio-technology, creative, medical, education and tourism hubs. Private investments have taken over public investment as the main source of funding to develop the region. This is especially evident in Medini, where capital investments have been allocated to soft infrastructure projects such as healthcare (Gleneagles Hospital), education (Educity™), lifestyle (Legoland™), and creative (Pinewood Studios).</p>

                                        <p>Besides integrated infrastructures coupled with high-quality education, healthcare and other first-rate community facilities, Medini offers a high standard of living at a much lower cost in comparison with Singapore, Hong Kong, Taiwan, Japan and Korea. With the support of the Malaysian government, it offers development opportunities that are more cost-efficient than any other established cities in Asia.</p>
                                    </article>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php require_once(dirname(__FILE__) . '/footer.php'); ?>
