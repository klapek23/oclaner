<?php require_once(dirname(__FILE__) . '/header.php'); ?>

    <section class="main-banner" style="background-image: url('/files/banner_wealth_management.jpg');">
        <div class="content">
            <article class="rounded">
                <h3>OCLANER WEALTH MANAGEMENT</h3>
                <h2>Create your <br>
                    growth story</h2>
                <p>Discover Oclaner Funds</p>
            </article>
        </div>
    </section>

<section class="content-section standard-content dark-bg no-padding-bottom">
    <article>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Asset Management</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>As a Singapore-based and Capital Markets-licensed Asset Management firm regulated by the Monetary Authority of Singapore, our investment management arm offers comprehensive and independent solutions to both individual and institutional clients as well as bespoke portfolio constructions for individuals, families, institutions and corporations.</p>
                </div>
            </div>
        </div>
    </article>
</section>

    <section class="content-section content-tabs-module page-tabs-module">
        <nav class="tabs-nav pager">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                            <li><span>Oclaner Mandates</span></li>
                            <li><span>Oclaner Funds UCITS IV SICAV</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="tabs-content slider-content">
            <section class="slide">
                <div class="content-section standard-content no-padding-bottom"">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Create your growth story</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>We offer individual and institutional clients four distinct UCITS IV compliant funds with four distinct strategies for different risk appetites. They have been developed, tested and fine-tuned to respond to specific needs be it gaining exposure to the fastest growing region in the world or to the top global companies. Regardless of the security, geography or industry we look at, understanding and minimising risk is our single most important preoccupation. All four funds are managed with our Oclaner signature: an active, flexible and conservative approach to investing.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="content-section accordion-module simply-accordion-module no-padding-top" >
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="accordion-content">
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Asia Pacific Equity Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                        </div>
                                        <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Provident World Equity Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                        <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Asian Bond Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Patrimonial Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="slide">
                <div class="content-section standard-content no-padding-bottom"">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Create your growth story</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>We offer individual and institutional clients four distinct UCITS IV compliant funds with four distinct strategies for different risk appetites. They have been developed, tested and fine-tuned to respond to specific needs be it gaining exposure to the fastest growing region in the world or to the top global companies. Regardless of the security, geography or industry we look at, understanding and minimising risk is our single most important preoccupation. All four funds are managed with our Oclaner signature: an active, flexible and conservative approach to investing.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="content-section accordion-module simply-accordion-module no-padding-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="accordion-content">
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Asia Pacific Equity Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                        </div>
                                        <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Provident World Equity Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Asian Bond Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Patrimonial Fund</span>
                                            <article>
                                                <p>The Oclaner Asia Pacific Equity Fund leverages on Asia’s growth story while mitigating the volatility of the stock markets</p>
                                            </article>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>

    <section class="content-section commentary-section dark-bg" ng-app="commentaryApp" ng-controller="appCtrl">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="commentary-nav" ng-controller="navCtrl">
                        <ul>
                            <li ng-repeat="item in categories" ng-class="{'active' : activeCategoryId == item.id }"><span data-name="{{item.id}}" ng-click="changeOnClick($event)">{{item.name}}</span></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="commentary-content">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="filters accordion-module simply-accordion-module" ng-controller="filtersCtrl">
                                    <div class="accordion-content">
                                        <h3>Filters</h3>
                                        <section class="item" ng-repeat="filter in filters">
                                            <div class="inside">
                                                <span class="name">{{ filter.title }}</span>
                                                <article>
                                                    <ul>
                                                        <li ng-click="setActiveFilter(filter.name, '')"><span>All</span></li>
                                                        <li ng-repeat="item in filter.items | orderBy: item : true" ng-class="{'active' : activeFilters[filter.name] == item }"><span ng-click="filterByElement($event, filter.name)" data-filter-value="{{item}}">{{item}}</span></li>
                                                    </ul>
                                                </article>
                                            </div>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <section class="commentary-list" ng-controller="listCtrl">
                                    <article class="item" ng-repeat="item in items | toArray | filter: { year: activeFilters.years, subcategory: activeFilters.subcategories } | orderBy: year : true">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <h4>{{item.name}}</h4>
                                                <p>{{item.description}}</p>
                                                <em>{{item.subcategory}}</em>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="page-data">
                                                    <ul>
                                                        <li><a href="{{item.file}}" title="">
                                                                <i class="icon icon_loop-icon"></i>
                                                                <em>View</em>
                                                            </a></li>
                                                        <li><a href="{{item.file}}" title="">
                                                                <i class="icon icon_download-icon"></i>
                                                                <em>Download</em>
                                                            </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php require_once(dirname(__FILE__) . '/footer.php'); ?>
