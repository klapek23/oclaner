<footer id="main-footer">
    <p>© 2015 Oclaner Asset Management Pte Ltd (Company Registration No. 200917644E) is regulated and supervised by the Monetary Authority of Singapore. We hold a Capital Markets Services Licence under the Securities and Futures Act to conduct fund management activities for accredited investors and institutional investors, as defined under the Securities and Futures Act, Singapore. <a href="#" title="Privacy policy">Privacy policy</a></p>
</footer>

</body>
</html>
