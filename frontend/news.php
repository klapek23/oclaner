<?php require_once(dirname(__FILE__) . '/header.php'); ?>
	
    <div class="main-banner" style="background-image: url('/files/banner_news.jpg');">
        <div class="content">
            <article class="rounded">
                <h3>NEWS</h3>
                <h2>Updates from <br>
                    Oclaner.</h2>
                <p>with Oclaner Family Office </p>
            </article>
        </div>
    </div>

    <section ng-app="newsApp" ng-controller="AppCtrl" ng-class="['content-section', appClass]">
        <div class="container">
            <div class="row">
                <div ng-class="{ 'col-md-8' : appView == 'list', 'col-md-12' : appView == 'single' }">
                    <h2>News / Press / Events</h2>
                </div>
                <div class="col-md-4 news-years-filter" ng-if="appView == 'list'">
                    <nav class="filter" ng-controller="FiltersCtrl">
                        <span class="title">Year:</span>
                        <ul>
                            <li ng-click="filterByYear('');">All</li>
                            <li ng-repeat="year in years" ng-click="filterByElement($event, 'year');">{{ year }}</li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row news-back-button" ng-if="appView == 'single'">
                <div class="col-md-12">
                    <div class="news-top-nav">
                        <a href="#" ng-click="back();">
                            <i class="icon icon_arrow-left"></i> Back
                        </a>
                    </div>
                </div>
            </div>
            <div class="newsContainer">
                <div class="loader"><div class="table"><div class="cell"><i class="icon icon_spinner-icon"></i></div></div></div>
                <div class="newsAnimate" ng-view news-animate></div>
            </div>
        </div>
    </section>

<?php require_once(dirname(__FILE__) . '/footer.php'); ?>