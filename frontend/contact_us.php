<?php require_once(dirname(__FILE__) . '/header.php'); ?>
	
    <div class="main-banner" style="background-image: url('/files/banner_contact_us.jpg');">
        <div class="content">
            <article class="rounded">
                <h3>CONTACT US</h3>
                <h2>Get in touch <br>
                    with us</h2>
            </article>
        </div>
    </div>

    <section class="content-section standard-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>If you would like to find out more about our services we would be pleased to provide you with additional information.
                        All communication will be handled strictly private and confidential.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="content-section contact-module">
        <div class="container">
            <div class="col-md-4">
                <table>
                    <tr>
                        <td><strong>Address:</strong></td>
                        <td>#37 – 04/05 Millenia Tower, 1 Temasek Avenue, Singapore 039192</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4">
                <table>
                    <tr>
                        <td><strong>Phone: </strong></td>
                        <td><strong>Fax: </strong></td>
                    </tr>
                    <tr>
                        <td>+65 6643 5110</td>
                        <td>+65 6336 2805</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4">
                <table>
                    <tr>
                        <td>For any further assistance </td>
                    </tr>
                    <tr>
                        <td>email us at: <a href="mailto:enquiries@oclaner.com" title="enquiries@oclaner.com"><strong>enquiries@oclaner.com</strong></a></td>
                    </tr>
                </table>
            </div>
        </div>
    </section>

<?php require_once(dirname(__FILE__) . '/footer.php'); ?>