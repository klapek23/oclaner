<?php require_once(dirname(__FILE__) . '/header.php'); ?>
	
    <div class="main-banner" style="background-image: url('/files/banner_news.jpg');">
        <div class="content">
            <article class="rounded">
                <h3>NEWS</h3>
                <h2>Updates from <br>
                    Oclaner.</h2>
                <p>with Oclaner Family Office </p>
            </article>
        </div>
    </div>

    <section class="content-section single-news-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>News / Press / Events</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="news-top-nav">
                        <a href="/news" title="">
                            <i class="icon icon_arrow-left"></i> Back
                        </a>
                    </div>
                </div>
            </div>
            <section class="single-news">
                <div class="news-data">May 08-05, 2015, by Jeremy Grant</div>
                <h3>Role of Asian Family Offices expands from a narrow base</h3>
                <article>
                    <p>Given the scale of wealth in Asia – in value and numbers of rich families – it would seem reasonable to assume that family offices are entrenched in the region. Credit Suisse estimates that globally there are close to 34,000 “ultra-high-net-worth” individuals, which it defines as those with a net wealth of $100m or more. About a quarter of those are in Asia.</p>
                    <p>Yet family offices have been rare. Credit Suisse reports that, of an estimated 3,000 single family offices globally, only 3 per cent are in Asia. At least half of them were set up in the past 15 years, which is not long considering the multi-generational history of their equivalents in Europe.<br>
                        Solving family conflicts, ensuring that wealth is transferred to future generations, consolidating assets, and – increasingly in Asia – dispensing advice on charitable activities are all part of the role of a family office.</p>
                    <p>“In the west, family offices are more mature and clearly defined. They are much more structured compared with Asian family offices,” says Dharuma Tharu, chief operating officer at Oclaner, a Swiss-based multifamily office that set up in Singapore in 2009. Yet family offices are becoming seen as not only desirable, but essential, by the growing ranks of wealthy families in Asia.</p>
                    <p>“The phrase ‘family office’ has gained a bit more traction over the past couple of years and a lot of Asian families are becoming more open to having such structures,” Mr Tharu explains.</p>
                </article>
                <section class="social-box">
                    <a href="#" title="Share on LinkedIn"><i class="icon icon_linkedin"></i><em>Share on linkedin</em></a>
                </section>
            </section>
        </div>
    </section>

<?php require_once(dirname(__FILE__) . '/footer.php'); ?>