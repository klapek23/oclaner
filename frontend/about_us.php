<?php require_once(dirname(__FILE__) . '/header.php'); ?>
	
    <div class="main-banner" style="background-image: url('/files/banner_about_us.jpg');">
        <div class="content">
            <article class="rounded">
                <h3>ABOUT US</h3>
                <h2>Independence. <br>
                    Access.<br>
                    Transparency.</h2>
                <p>A transparent, open architecture. Access to the best advisors covering legal, tax, insurance, estate planning and investment matters.  </p>
            </article>
        </div>
    </div>

    <section class="content-section standard-content">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>About Oclaner</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>Oclaner Asset Management is a Singapore-based asset management firm and Capital Markets Services license holder (issued by Monetary Authority of Singapore) with a growing team of industry experts and international partners.</p>
                            <p>Possessing a wealth of know-how that spans over 30 years of working with ultra high net worth families and institutions globally, we draw from our extensive network and experience to offer 3 comprehensive services that include Family Office Advisory, Investment Management and Direct Investments.</p>
                    </div>
                </div>
            </div>
        </article>
    </section>

    <section class="content-section tabs-module border-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="tabs-nav pager">
                        <ul>
                            <li><span>Family Office Advisory</span> <hr></li>
                            <li><span>Investment Management</span> <hr></li>
                            <li><span>Direct Investments</span> <hr></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs-content slider-content">
                        <article class="slide"><p>With a dedicated structure and distinctive model in place, we cover our clients specific needs by empowering them with control over the management and objectives of their family office as well as focusing on wealth preservation and trans-generational planning through Trust & Legal Coordination (Estate Planning), Financial Consolidation & Reporting, Investment Management and Bridging (Direct Investments).</p><p>With a dedicated structure and distinctive model in place, we cover our clients specific needs by empowering them with control over the management and objectives of their family office as well as focusing on wealth preservation and trans-generational planning through Trust & Legal Coordination (Estate Planning), Financial Consolidation & Reporting, Investment Management and Bridging (Direct Investments).</p></article>
                        <article class="slide"><p>With a dedicated structure and distinctive model in place, we cover our clients specific needs by empowering them with control over the management and objectives of their family office as well as focusing on wealth preservation and trans-generational planning through Trust & Legal Coordination (Estate Planning), Financial Consolidation & Reporting, Investment Management and Bridging (Direct Investments).</p></article>
                        <article class="slide"><p>With a dedicated structure and distinctive model in place, we cover our clients specific needs by empowering them with control over the management and objectives of their family office as well as focusing on wealth preservation and trans-generational planning through Trust & Legal Coordination (Estate Planning), Financial Consolidation & Reporting, Investment Management and Bridging (Direct Investments).</p></article>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-section accordion-module">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>The Board</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="accordion-content">
                        <section class="item">
                            <div class="row">
                                <div class="col-md-2 col-sm-2">
                                    <img src="/files/the_board.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <div class="inside">
                                        <span class="name">Ivo Bartoletti</span>
                                        <p class="short-info">Executive Director, Chairman <br>
                                            & Chief Executive Officer</p>
                                        <article>
                                            <p>Mr. Ivo Bartoletti is the founder, Executive Director, Chairman & Chief Executive Officer of Oclaner Asset Management Pte Ltd, which he established in 2009 Mr. Bartoletti started his banking career in 1980 with Banco Di Roma International SA, Luxembourg. In 1981, he was appointed Head of the Money Market and Forward desk. From 1983 to 1986, he successfully led the foreign exchange desk. Thereafter, Mr. Bartoletti joined Commerzbank International SA, Luxembourg as Chief Trader for Currency and Options.</p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                        </section>
                        <section class="item">
                            <div class="row">
                                <div class="col-md-2 col-sm-2">
                                    <img src="/files/the_board.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <div class="inside">
                                        <span class="name">Ivo Bartoletti</span>
                                        <p class="short-info">Executive Director, Chairman <br>
                                            & Chief Executive Officer</p>
                                        <article>
                                            <p>Mr. Ivo Bartoletti is the founder, Executive Director, Chairman & Chief Executive Officer of Oclaner Asset Management Pte Ltd, which he established in 2009 Mr. Bartoletti started his banking career in 1980 with Banco Di Roma International SA, Luxembourg. In 1981, he was appointed Head of the Money Market and Forward desk. From 1983 to 1986, he successfully led the foreign exchange desk. Thereafter, Mr. Bartoletti joined Commerzbank International SA, Luxembourg as Chief Trader for Currency and Options.</p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                        </section>
                    </div>
                </div>
            </div>
    </section>

<?php require_once(dirname(__FILE__) . '/footer.php'); ?>