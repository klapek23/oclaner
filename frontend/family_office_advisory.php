<?php require_once(dirname(__FILE__) . '/header.php'); ?>

    <section class="banner-slider">
        <div class="slider-content">
            <div class="main-banner slide" style="background-image: url('/files/banner_family_office_advisory.jpg');">
                <div class="content">
                    <article class="rounded">
                        <h3>FAMILY OFFICE ADVISORY</h3>
                        <h2>Start your <br>
                            legacy</h2>
                        <p>with Oclaner Family Office</p>
                    </article>
                </div>
            </div>
            <div class="main-banner slide" style="background-image: url('/files/banner_sports_office_advisory.jpg');">
                <div class="content">
                    <article class="rounded">
                        <h3>SPORTS OFFICE ADVISORY</h3>
                        <h2>Stay ahead <br>
                            of the game</h2>
                        <p>with Oclaner Sports Office</p>
                    </article>
                </div>
            </div>
        </div>
    </section>

    <section class="content-section content-tabs-module page-tabs-module no-padding-bottom">
        <nav class="tabs-nav pager">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                            <li><span>Family Office Advisory</span></li>
                            <li><span>Sports Office Advisory</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="tabs-content slider-content">
            <section class="slide">
                <div class="content-section standard-content">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <h2>Family Office Advisory</h2>
                                </div>
                                <div class="col-md-4">
                                    <div class="page-data">
                                        <span class="title">Oclaner Family Office Brochure</span>
                                        <ul>
                                            <li><a href="#" title="">
                                                    <i class="icon icon_loop-icon"></i>
                                                    <em>View</em>
                                            </a></li>
                                            <li><a href="#" title="">
                                                    <i class="icon icon_download-icon"></i>
                                                    <em>Download</em>
                                                </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>A transparent, open architecture. Access to the best advisors covering legal, tax, insurance, estate planning and investment matters. At Oclaner we are able to offer you independence through our Family Office paradigm that manages investments using a multi-custodian platform.</p>
                                    <p>This distinctive model enables you to retain control over all your wealth-related financial affairs through the appointment of family representatives on the board, based on predetermined governance and investment policies set up during its inception.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="content-section accordion-module simply-accordion-module no-padding-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Our Services</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="accordion-content">
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Tax & Legal</span>
                                            <article>
                                                <p>We offer a comprehensive approach to tax optimisation strategies by co-ordinating with service providers ranging from trustees and lawyers on a global level to ensure that:</p>
                                                <ul>
                                                    <li>Your assets are structured in the most tax-optimum manner</li>
                                                    <li>Your estate is planned from an intergenerational perspective</li>
                                                </ul>
                                                <p>In addition, we supervise the setting up of a proper governance structure by outlining the responsibilities of all the decision makers in your family. This is done to address responsibility, authority and accountability.</p>
                                            </article>
                                        </div>
                                        <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Tax & Legal</span>
                                            <article>
                                                <p>We offer a comprehensive approach to tax optimisation strategies by co-ordinating with service providers ranging from trustees and lawyers on a global level to ensure that:</p>
                                                <ul>
                                                    <li>Your assets are structured in the most tax-optimum manner</li>
                                                    <li>Your estate is planned from an intergenerational perspective</li>
                                                </ul>
                                                <p>In addition, we supervise the setting up of a proper governance structure by outlining the responsibilities of all the decision makers in your family. This is done to address responsibility, authority and accountability.</p>
                                            </article>
                                        </div>
                                        <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="slide">
                <div class="content-section standard-content">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    <h2>Sports Office Advisory</h2>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="page-data">
                                        <span class="title">Oclaner Sports Office Brochure</span>
                                        <ul>
                                            <li><a href="#" title="">
                                                    <object type="image/svg+xml" data="/files/loop-icon.svg"></object>
                                                    <em>View</em>
                                                </a></li>
                                            <li><a href="#" title="">
                                                    <object type="image/svg+xml" data="/files/download-icon.svg"></object>
                                                    <em>Download</em>
                                                </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Building on years of experience as a multi-family office and combining it to our passion for sports, we have developed the Oclaner Sports Office, a model tailored to the needs of athletes worldwide. Our Sports Office structure is designed to simplify the complexities encountered by athletes to enable them to focus on their careers and provide them peace of mind.</p>
                                </div>
                            </div>
                    </article>
                </div>

                <div class="content-section accordion-module simply-accordion-module no-padding-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Our Services</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="accordion-content">
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Tax & Legal</span>
                                            <article>
                                                <p>We offer a comprehensive approach to tax optimisation strategies by co-ordinating with service providers ranging from trustees and lawyers on a global level to ensure that:</p>
                                                <ul>
                                                    <li>Your assets are structured in the most tax-optimum manner</li>
                                                    <li>Your estate is planned from an intergenerational perspective</li>
                                                </ul>
                                                <p>In addition, we supervise the setting up of a proper governance structure by outlining the responsibilities of all the decision makers in your family. This is done to address responsibility, authority and accountability.</p>
                                            </article>
                                        </div>
                                        <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name">Tax & Legal</span>
                                            <article>
                                                <p>We offer a comprehensive approach to tax optimisation strategies by co-ordinating with service providers ranging from trustees and lawyers on a global level to ensure that:</p>
                                                <ul>
                                                    <li>Your assets are structured in the most tax-optimum manner</li>
                                                    <li>Your estate is planned from an intergenerational perspective</li>
                                                </ul>
                                                <p>In addition, we supervise the setting up of a proper governance structure by outlining the responsibilities of all the decision makers in your family. This is done to address responsibility, authority and accountability.</p>
                                            </article>
                                        </div>
                                        <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    </section>

<?php require_once(dirname(__FILE__) . '/footer.php'); ?>
