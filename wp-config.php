<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'oclaner');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5.+~969k;QRHS}G<zSf)]euu+o@b8}g/YWo<nbPB%r[@)U`-?HSqTq QxsDKwR|,');
define('SECURE_AUTH_KEY',  '|szKC+Yt#g=|OV-[w6D3?o)ly|E7IHL.GQY6`[+x&zRu|jueeXVJ-1cB]zf ~J}2');
define('LOGGED_IN_KEY',    'rAs(zkFOXke!9-:m<3XUX~yfSLI]IA?+c$bBxOz|*d1Kw[tfpJ6^)KV#1Ln-zm^1');
define('NONCE_KEY',        '+Z rp;4DP>%8-6~).JPW|>f=[ZlwH<#Pc<Y~%-eGB<~b8*]dHxy$=gA*wRCfG@|:');
define('AUTH_SALT',        'I0Nv{s%j-?]x|$Ro94-MEHrm.xpIxJ r0#Sf9vHZdM+m2*$xCjA?.^tDD%3_U$x`');
define('SECURE_AUTH_SALT', '%^_nDUi4BAQIR`|S5p||Y`]|-CO,2u=S]OC~q-xf4AY*8e5nALKob+0jE>IqSAt!');
define('LOGGED_IN_SALT',   'Zr!OCXiA5OEb3N.s7=-:hHN8n[u#>PqN>R[[%`e|/bz.p{JP-boZ+|<UXWqNqKJm');
define('NONCE_SALT',       'B4nyMB|tEiPZ6SVf.U:%Ou=[ZNg/mELAw.l|5O$Z A6KRVuY|ooTb=5K!Oiu{yt~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
