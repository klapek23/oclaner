<?php

namespace PostTypeWealthManagement;

class PostTypeWealthManagementClass {

    public function subcategory_taxonomy() {

        $labels = array(
            'name'              => _x( 'Subcategories', 'klapek23_framework' ),
            'singular_name'     => _x( 'Subcategory', 'klapek23_framework' ),
            'search_items'      => __( 'Search Subcategories' ),
            'all_items'         => __( 'All Subcategories' ),
            'parent_item'       => __( 'Parent Subcategory' ),
            'parent_item_colon' => __( 'Parent Subcategory:' ),
            'edit_item'         => __( 'Edit Subcategory' ),
            'update_item'       => __( 'Update Subcategory' ),
            'add_new_item'      => __( 'Add New Subcategory' ),
            'new_item_name'     => __( 'New Subcategory Name' ),
            'menu_name'         => __( 'Subcategories' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'subcategory' ),
            'show_in_rest'       => true,
            'rest_base'          => 'subcategory',
            'rest_controller_class' => 'WP_REST_Terms_Controller',
        );

        register_taxonomy( 'subcategory', 'wealth-management-items', $args );

    }



    //create post type
	public function createPostType() {

		$labels = array(
			'name'               => _x( 'Asset Management', 'klapek23_framework' ),
			'singular_name'      => _x( 'Asset Management', 'klapek23_framework' ),
			'add_new'            => _x( 'Add new', 'klapek23_framework' ),
			'add_new_item'       => __( 'Add new item', 'klapek23_framework' ),
			'edit_item'          => __( 'Edit item', 'klapek23_framework' ),
			'new_item'           => __( 'New', 'klapek23_framework'),
			'all_items'          => __( 'All items', 'klapek23_framework' ),
			'view_item'          => __( 'Show item', 'klapek23_framework' ),
			'search_items'       => __( 'Show all items', 'klapek23_framework' ),
			'not_found'          => __( 'No results', 'klapek23_framework' ),
			'not_found_in_trash' => __( 'No results in trash', 'klapek23_framework' ),
			'parent_item_colon'  => '',
			'menu_name'          => 'Asset Management'
		);

		$args = array(
			'labels'        => $labels,
            'description'        => __( 'Wealth Management.', 'klapek23_framework' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'show_in_rest'       => true,
            'rest_base'          => 'wealth-management-items',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
            'supports'          => array( 'title', 'editor', 'revisions', 'taxonomies' ),
            'rewrite'		    => array(
                'with_front'    => false,
                'slug' => 'wealth-management-items'
            ),
            'taxonomies'        => array('category', 'subcategory')
        );

		register_post_type( 'wealth-management', $args );

		flush_rewrite_rules();
	}


    public function register_plugin_styles() {

        wp_register_style( 'post-type-offer-plugin', plugins_url( 'news-plugin/css/plugin.css' ) );
        wp_enqueue_style( 'post-type-offer-plugin' );

    }


    public function register_plugin_scripts() {

        wp_register_script( 'post-type-offer-plugin', plugins_url( 'post-type-offer-plugin/js/display.js' ) );
        wp_enqueue_script( 'post-type-offer-plugin' );

    }

}

?>