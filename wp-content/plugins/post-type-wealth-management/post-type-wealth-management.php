<?php

/*
Plugin Name: Wealth Management post type
Text Domain: wealth-management-plugin
Domain Path: /languages
Description: Plugin for display wealth-management
Version: 1.0
Author: klapek23
*/

namespace PostTypeWealthManagement;

//create namespace constant
define(__NAMESPACE__ . '\NS', __NAMESPACE__ . '\\');

define(NS . 'POST_TYPE_WEALTH_MANAGEMENT_DIR_URL', plugin_dir_url( __FILE__ ));
define(NS . 'POST_TYPE_WEALTH_MANAGEMENT_DIR_PATH', plugin_dir_path( __FILE__ ));


//load plugin textdomain
load_plugin_textdomain( 'post-type-news', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

//require main plugin class
require_once(POST_TYPE_WEALTH_MANAGEMENT_DIR_PATH . 'classes/PostTypeWealthManagementClass.php');
$init = new PostTypeWealthManagementClass();

add_action('init', array($init, 'subcategory_taxonomy'));
add_action('init', array($init, 'createPostType'));

?>