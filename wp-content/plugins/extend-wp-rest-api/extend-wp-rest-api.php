<?php
/*
Plugin Name: Extend WP-REST API
Text Domain: extend-wp-rest-api
Domain Path: /languages
Description: Extend WP-REST API
Version: 1.0
Author: klapek23
*/


add_action( 'rest_api_init', 'register_new_api_fields' );

function register_new_api_fields() {
    register_api_field( 'news',
        'thumbnail',
        array(
            'get_callback'    => 'get_post_thumb',
            'update_callback' => null,
            'schema'          => null,
        )
    );

    register_api_field( 'news',
        'author_name',
        array(
            'get_callback'    => 'get_post_author',
            'update_callback' => null,
            'schema'          => null,
        )
    );

    register_api_field( 'wealth-management',
        'subcategory',
        array(
            'get_callback'    => 'get_subcategory',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}


function get_post_thumb( $object, $field_name, $request ) {
    $thumb_id = get_post_thumbnail_id($object[ 'id' ]);
    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
    $thumb_url = $thumb_url_array[0];

    return $thumb_url;
}

function get_post_author( $object, $field_name, $request ) {
    $post = get_post($object[ 'id' ]);
    $author = $post->post_author;
    $author_name = get_the_author_meta('user_nicename', $author);

    return $author_name;
}

function get_subcategory( $object, $field_name, $request ) {
    $subcategories = get_the_terms($object[ 'id' ], 'subcategory');
    $subcategory = $subcategories[0]->name;

    return $subcategory;
}
?>