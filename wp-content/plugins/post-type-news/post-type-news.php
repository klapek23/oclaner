<?php

/*
Plugin Name: News post type
Text Domain: news-plugin
Domain Path: /languages
Description: Plugin for display news
Version: 1.0
Author: klapek23
*/

namespace PostTypeNews;

//create namespace constant
define(__NAMESPACE__ . '\NS', __NAMESPACE__ . '\\');

define(NS . 'POST_TYPE_NEWS_DIR_URL', plugin_dir_url( __FILE__ ));
define(NS . 'POST_TYPE_NEWS_DIR_PATH', plugin_dir_path( __FILE__ ));


//load plugin textdomain
load_plugin_textdomain( 'post-type-news', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

//require main plugin class
require_once(POST_TYPE_NEWS_DIR_PATH . 'classes/PostTypeNewsClass.php');
$init = new PostTypeNewsClass();

add_action('init', array($init, 'createPostType'));

/*if ( is_admin() ) {

	//require plugin admin class
	require_once(POST_TYPE_NEWS_DIR_PATH . 'classes/AdminClass.php');
	$adminPlugin = new AdminClass();

	add_action('admin_print_scripts', array($adminPlugin, 'post_type_news_admin_scripts'));
	add_action('admin_print_styles', array($adminPlugin, 'post_type_news_admin_styles'));

}*/

?>