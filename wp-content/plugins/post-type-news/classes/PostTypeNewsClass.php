<?php

namespace PostTypeNews;

class PostTypeNewsClass {


	//create post type
	public function createPostType() {

		$labels = array(
			'name'               => _x( 'News', 'klapek23_framework' ),
			'singular_name'      => _x( 'News', 'klapek23_framework' ),
			'add_new'            => _x( 'Add new', 'klapek23_framework' ),
			'add_new_item'       => __( 'Add new item', 'klapek23_framework' ),
			'edit_item'          => __( 'Edit news', 'klapek23_framework' ),
			'new_item'           => __( 'New', 'klapek23_framework'),
			'all_items'          => __( 'All news', 'klapek23_framework' ),
			'view_item'          => __( 'Show news', 'klapek23_framework' ),
			'search_items'       => __( 'Show all news', 'klapek23_framework' ),
			'not_found'          => __( 'No results', 'klapek23_framework' ),
			'not_found_in_trash' => __( 'No results in trash', 'klapek23_framework' ),
			'parent_item_colon'  => '',
			'menu_name'          => 'News'
		);

		$args = array(
			'labels'        => $labels,
            'description'        => __( 'News.', 'news' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'show_in_rest'       => true,
            'rest_base'          => 'news',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
            'supports'          => array( 'title', 'editor', 'revisions', 'thumbnail' ),
            'rewrite'		    => array(
                'with_front'    => false,
                'slug' => 'news'
            )
        );

		register_post_type( 'news', $args );

		flush_rewrite_rules();
	}


    public function register_plugin_styles() {

        wp_register_style( 'post-type-offer-plugin', plugins_url( 'news-plugin/css/plugin.css' ) );
        wp_enqueue_style( 'post-type-offer-plugin' );

    }


    public function register_plugin_scripts() {

        wp_register_script( 'post-type-offer-plugin', plugins_url( 'post-type-offer-plugin/js/display.js' ) );
        wp_enqueue_script( 'post-type-offer-plugin' );

    }

}

?>