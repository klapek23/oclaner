<?php

namespace PostTypeNews;

class AdminClass {

    //add scripts to admin plugin page
    function post_type_offer_admin_scripts() {
        if (isset($_GET['page']) && $_GET['page'] == 'news-plugin-add') {
            wp_enqueue_script('jquery');
            wp_enqueue_script('media-upload');
            wp_enqueue_script('thickbox');
            wp_register_script('my-upload', TEST_PLUGIN_DIR_URL.'js/admin-script.js', array('jquery','media-upload','thickbox'));
            wp_enqueue_script('my-upload');
        }
    }


    //add styles to admin plugin page
    function post_type_offer_admin_styles() {
        if (isset($_GET['page'])) {
            if($_GET['page'] == 'news-plugin-add' || $_GET['page'] == 'news-plugin-menu') {
               wp_enqueue_style('thickbox');
               wp_register_style('test-plugin-admin', TEST_PLUGIN_DIR_URL.'css/admin-script.css');
               wp_enqueue_style('test-plugin-admin');
            }
        }
    }

}

?>