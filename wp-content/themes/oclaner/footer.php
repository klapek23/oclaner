<footer id="main-footer">
    <?php dynamic_sidebar('bottom-site-info'); ?>

    <?php wp_footer(); ?>
</footer>

<?php /*if(!isset( $_COOKIE['cookies_accept'])): */?><!--
    <div class="cookies_info">
        <section class="content">
            <div class="close">X</div>
            <i class="icon icon_cookie-icon"></i>
            <p><?php /*_e('This site use cookies...', 'klapek23_framework'); */?></p>
            <div class="clearfix"></div>
            <a href="<?php /*echo get_option('cookies-link'); */?>" class="button" ><?php /*_e('Read more', 'klapek23_framework'); */?></a>
        </section>
    </div>

    <script>
        jQuery(document).ready(function() {
            var $cokies = jQuery('.cookies_info'),
                cokiesPopup = new cookies();

            cokiesPopup.enablePopup($cokies, jQuery('.close'));
            cokiesPopup.showPopup();

            jQuery(cokiesPopup).on('hide-popup-stop', function() {
                jQuery.cookie('cookies_accept', 1, {path: '/', expires: 365});
            });
        });
    </script>
--><?php /*endif; */?>

</body>
</html>
