<?php
/* ----------
Template name: About Us
------------ */
?>

<?php get_header(); ?>

<?php
$mainBanner = get_field('main_banner_image');
$mainBannerImage = $mainBanner['url'];
?>

    <div class="main-banner" data-width="1920" data-height="1200" style="background-image: url(<?php echo $mainBannerImage; ?>);">
        <div class="content">
            <article class="rounded">
                <h3><?php the_field('main_banner_subtitle'); ?></h3>
                <h2><?php the_field('main_banner_title'); ?></h2>
                <?php /*if(get_field('main_banner_text')): */?><!--
                    <p><?php /*the_field('main_banner_text'); */?></p>
                --><?php /*endif; */?>
            </article>
        </div>
    </div>

    <section class="content-section standard-content">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?php the_field('about_oclaner_title'); ?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><?php the_field('about_oclaner_content'); ?></div>
                </div>
            </div>
        </article>
    </section>

    <?php if( have_rows('tabs') ): ?>
    <section class="content-section tabs-module border-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="tabs-nav pager">
                        <ul>
                            <?php while( have_rows('tabs') ): the_row(); ?>
                                <li><span><?php the_sub_field('title'); ?></span> <hr></li>
                            <?php endwhile; ?>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs-content slider-content">
                        <?php while( have_rows('tabs') ): the_row(); ?>
                            <article class="slide"><?php the_sub_field('content'); ?></p>
                                <p class="more"><a href="<?php the_sub_field('find_out_more_link'); ?>" title="<?php _e('Find out more...', 'klapek23_framework'); ?>" ><?php _e('Find out more...', 'klapek23_framework'); ?></a></p>
                            </article>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <section class="content-section accordion-module">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2><?php the_field('board_title'); ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="accordion-content">
                        <?php while( have_rows('board_items') ): the_row(); ?>
                            <?php
                                $itemImage = get_sub_field('image');
                                $itemImage = $itemImage['url'];
                            ?>
                            <section class="item">
                                <div class="image-container">
                                    <img src="<?php echo $itemImage; ?>" alt="<?php the_sub_field('name'); ?>" class="img-responsive">
                                </div>
                                <div class="text-container">
                                    <div class="inside">
                                        <span class="name"><?php the_sub_field('name'); ?></span>
                                        <p class="short-info"><?php the_sub_field('description'); ?></p>
                                        <article>
                                            <?php the_sub_field('content'); ?>
                                        </article>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                            </section>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
    </section>

<?php get_footer(); ?>