<?php
/* ----------
Template name: Family Office Advisory
------------ */
?>

<?php get_header(); ?>

<?php
/*$pages = get_children(array(
    'post_parent'   =>  $post->ID,
    'post_type'     =>  'page',
    'post_status'   =>  'publish'
));*/

$pages = get_field('tabs');
?>

    <?php if(!empty($pages)): ?>
    <section class="banner-slider">
        <div class="slider-content">
            <?php foreach($pages as $page):
                $page = $page['page'];
                $mainBanner = get_field('main_banner_image', $page->ID);
                $mainBannerImage = $mainBanner['url'];
            ?>
            <div class="main-banner slide" data-width="1920" data-height="1200" style="background-image: url(<?php echo $mainBannerImage; ?>);">
                <div class="content">
                    <article class="rounded">
                        <h3><?php the_field('main_banner_subtitle', $page->ID) ?></h3>
                        <h2><?php the_field('main_banner_title', $page->ID) ?></h2>
                        <?php /*if(get_field('main_banner_text', $page->ID)): */?><!--
                            <p><?php /*the_field('main_banner_text', $page->ID) */?></p>
                        --><?php /*endif; */?>
                    </article>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </section>
    <?php endif; ?>

    <?php if(!empty($pages)): ?>
    <section class="content-section content-tabs-module page-tabs-module no-padding-bottom">
        <nav class="tabs-nav pager">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                        <?php foreach($pages as $page):
                            $page = $page['page'];
                        ?>
                            <li><span><?php the_field('title', $page->ID) ?></span></li>
                        <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="tabs-content slider-content">

            <?php foreach($pages as $page):
                $page = $page['page'];
            ?>
            <section class="slide">
                <div class="content-section standard-content">
                    <article>
                        <div class="container">
                            <div class="row">
                                <h2><?php the_field('title', $page->ID) ?></h2>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><?php the_field('content', $page->ID); ?></div>
                            </div>
                        </div>
                    </article>

                    <div class="page-data">
                        <span class="title"><?php the_field('brochure_title', $page->ID) ?></span>
                        <ul>
                            <?php
                            $brochure = get_field('brochure_file', $page->ID);
                            ?>
                            <li><a href="<?php echo $brochure['url']; ?>" title="<?php _e('View', 'klapek23_framework'); ?>">
                                    <i class="icon icon_loop-icon"></i>
                                    <em><?php _e('View', 'klapek23_framework'); ?></em>
                                </a></li>
                            <li><a href="<?php echo $brochure['url']; ?>" title="<?php _e('Download', 'klapek23_framework'); ?>" download>
                                    <i class="icon icon_download-icon"></i>
                                    <em><?php _e('Download', 'klapek23_framework'); ?></em>
                                </a></li>
                        </ul>
                    </div>
                </div>

                <!--<div class="content-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                            </div>
                        </div>
                    </div>
                </div>-->

                <?php if( have_rows('accordion', $page->ID) ): ?>
                <div class="content-section accordion-module simply-accordion-module no-padding-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2><?php the_field('accordion_title'); ?></h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="accordion-content">
                                    <?php while( have_rows('accordion', $page->ID) ): the_row(); ?>
                                        <section class="item">
                                            <div class="inside">
                                                <span class="name"><?php the_sub_field('title'); ?></span>
                                                <article><?php the_sub_field('content'); ?></article>
                                            </div>
                                            <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                        </section>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </section>
            <?php endforeach; ?>

        </div>
    </section>

    <?php endif; ?>

<?php get_footer(); ?>
