<?php
/* ----------
Template name: Direct Investments
------------ */
?>

<?php get_header(); ?>

<?php
$mainBanner = get_field('main_banner_image');
$mainBannerImage = $mainBanner['url'];
?>

    <div class="main-banner" data-width="1920" data-height="1200" style="background-image: url(<?php echo $mainBannerImage; ?>);">
        <div class="content">
            <article class="rounded">
                <h3><?php the_field('main_banner_subtitle'); ?></h3>
                <h2><?php the_field('main_banner_title'); ?></h2>
                <?php /*if(get_field('main_banner_text')): */?><!--
                    <p><?php /*the_field('main_banner_text'); */?></p>
                --><?php /*endif; */?>
            </article>
        </div>
    </div>

    <section class="content-section standard-content">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?php the_field('direct_investments_title'); ?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><?php the_field('direct_investments_content'); ?></div>
                </div>
            </div>
        </article>
    </section>

    <?php if( have_rows('tabs') ): ?>
        <section class="content-section tabs-module border-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="tabs-nav pager">
                            <ul>
                                <?php while( have_rows('tabs') ): the_row(); ?>
                                    <li><span><?php the_sub_field('title'); ?></span> <hr></li>
                                <?php endwhile; ?>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs-content slider-content">
                            <?php while( have_rows('tabs') ): the_row(); ?>
                                <article class="slide"><?php the_sub_field('content'); ?></p></article>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <section class="content-section two-cols-module">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2><?php the_field('two_cols_content_title'); ?></h2>
                    <h3><?php the_field('two_cols_content_subtitle'); ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <article class="left-column">
                        <?php the_field('two_cols_content_left_column'); ?>
                    </article>
                </div>
                <div class="col-md-6 col-sm-6">
                    <article class="right-column">
                        <?php the_field('two_cols_content_right_column'); ?>
                    </article>
                </div>
            </div>
        </div>
    </section>

    <?php if( have_rows('bottom_tabs') ): ?>
    <section class="content-section content-tabs-module no-padding-top">
        <nav class="tabs-nav pager">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                            <?php while( have_rows('bottom_tabs') ): the_row(); ?>
                            <li><span><?php the_sub_field('title'); ?></span></li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs-content slider-content">
                        <?php while( have_rows('bottom_tabs') ): the_row(); ?>
                        <section class="slide">
                            <?php
                                $image = get_sub_field('image');
                            ?>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>" class="img-responsive hg-img-full-width">
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <h4><?php the_sub_field('subtitle'); ?></h4>
                                    <article><?php the_sub_field('content'); ?></article>
                                </div>
                            </div>
                        </section>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

<?php get_footer(); ?>
