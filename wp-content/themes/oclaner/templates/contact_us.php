<?php
/* ----------
Template name: Contact Us
------------ */
?>

<?php get_header(); ?>

<?php
$mainBanner = get_field('main_banner_image');
$mainBannerImage = $mainBanner['url'];
?>

    <div class="main-banner" data-width="1920" data-height="1200" style="background-image: url(<?php echo $mainBannerImage; ?>);">
        <div class="content">
            <article class="rounded">
                <h3><?php the_field('main_banner_subtitle'); ?></h3>
                <h2><?php the_field('main_banner_title'); ?></h2>
                <?php /*if(get_field('main_banner_text')): */?><!--
                    <p><?php /*the_field('main_banner_text'); */?></p>
                --><?php /*endif; */?>
            </article>
        </div>
    </div>

    <section class="content-section standard-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; else : ?>
                        <p><?php _e( 'Sorry, no result' ); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <section class="content-section contact-module">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <table>
                        <tr>
                            <td><strong><?php _e('Address:', 'klapek23_framework'); ?></strong></td>
                        </tr>

                        <tr>
                            <td><?php the_field('address'); ?></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4">
                    <table>
                        <tr>
                            <td><strong><?php _e('Phone:', 'klapek23_framework'); ?></strong></td>
                            <td><strong><?php _e('Fax:', 'klapek23_framework'); ?></strong></td>
                        </tr>
                        <tr>
                            <td><?php the_field('phone'); ?></td>
                            <td><?php the_field('fax'); ?></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4">
                    <table>
                        <tr>
                            <td><?php _e('For any further assistance', 'klapek23_framework'); ?></td>
                        </tr>
                        <tr>
                            <td><?php _e('email us at:', 'klapek23_framework'); ?> <a href="mailto:<?php the_field('email'); ?>" title="<?php the_field('email'); ?>"><strong><?php the_field('email'); ?></strong></a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>