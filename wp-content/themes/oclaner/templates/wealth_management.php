<?php
/* ----------
Template name: Wealth Management
------------ */
?>

<?php get_header(); ?>

<?php
$mainBanner = get_field('main_banner_image');
$mainBannerImage = $mainBanner['url'];
?>

    <div class="main-banner" data-width="1920" data-height="1200" style="background-image: url(<?php echo $mainBannerImage; ?>);">
        <div class="content">
            <article class="rounded">
                <h3><?php the_field('main_banner_subtitle'); ?></h3>
                <h2><?php the_field('main_banner_title'); ?></h2>
                <?php /*if(get_field('main_banner_text')): */?><!--
                    <p><?php /*the_field('main_banner_text'); */?></p>
                --><?php /*endif; */?>
            </article>
        </div>
    </div>

    <section class="content-section standard-content dark-bg no-padding-bottom">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?php the_field('asset_management_title'); ?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><?php the_field('asset_management_content'); ?></div>
                </div>
            </div>
        </article>
    </section>

    <?php if( have_rows('tabs') ): ?>
    <section class="content-section content-tabs-module page-tabs-module">
        <nav class="tabs-nav pager">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                            <?php while( have_rows('tabs') ): the_row(); ?>
                                <li><span><?php the_sub_field('title'); ?></span></li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="tabs-content slider-content">
            <?php while( have_rows('tabs') ): the_row(); ?>
            <section class="slide">
                <div class="content-section standard-content no-padding-bottom">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3><?php the_sub_field('content_title'); ?></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php the_sub_field('content'); ?>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>

                <?php if( have_rows('accordion') ): ?>
                <div class="content-section accordion-module simply-accordion-module no-padding-top" >
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="accordion-content">
                                    <?php while( have_rows('accordion') ): the_row(); ?>
                                    <section class="item">
                                        <div class="inside">
                                            <span class="name"><?php the_sub_field('title'); ?></span>
                                            <article style="display: block; padding-bottom: 10px">
                                                <p><?php the_sub_field('content'); ?></p>
                                            </article>
                                        </div>
                                    </section>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if(get_sub_field('title') == 'Oclaner Funds'): ?>
                <div class="content-section commentary-section dark-bg" ng-app="commentaryApp" ng-controller="appCtrl">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <nav class="commentary-nav" ng-controller="navCtrl">
                                    <ul>
                                        <li ng-repeat="item in categories" ng-class="{'active' : activeCategoryId == item.id }"><span data-name="{{item.id}}" ng-click="changeOnClick($event)">{{item.name}}</span></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="commentary-content">
                                    <div ng-class="{'loader' : true, 'active' : animate}"><div class="table"><div class="cell"><i class="icon icon_spinner-icon"></i></div></div></div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="filters accordion-module simply-accordion-module" ng-controller="filtersCtrl">
                                                <div class="accordion-content">
                                                    <section class="item" ng-repeat="filter in filters">
                                                        <div class="inside">
                                                            <span class="name">{{ filter.title }}</span>
                                                            <article>
                                                                <ul>
                                                                    <li ng-click="setActiveFilter(filter.name, '')"><span><?php _e('All', 'klapek23_framework'); ?></span></li>
                                                                    <li ng-repeat="item in filter.items | orderBy: sortOrder" ng-class="{'active' : activeFilters[filter.name] == item }"><span ng-click="filterByElement($event, filter.name);" data-filter-value="{{item}}">{{item}}</span></li>
                                                                </ul>
                                                            </article>
                                                        </div>
                                                        <span class="accordion-toggle"><i class="fa fa-angle-down"></i></span>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <section class="commentary-list" ng-controller="listCtrl">
                                                <article class="item" ng-repeat="item in items | toArray | filterByYear : activeFilters.years | filter: { subcategory: activeFilters.subcategories } | orderBy: date">
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <h4>{{item.title.rendered}}</h4>
                                                            <p ng-if="item.acf.show_file_name">{{item.acf.file.title}}</p>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="page-data">
                                                                <ul>
                                                                    <li><a href="{{item.acf.file.url}}" title="{{item.acf.file.title}}" target="_blank">
                                                                            <i class="icon icon_loop-icon"></i>
                                                                            <em><?php _e('View', 'klapek23_framework'); ?></em>
                                                                        </a></li>
                                                                    <li><a href="{{item.acf.file.url}}" title="{{item.acf.file.title}}" download>
                                                                            <i class="icon icon_download-icon"></i>
                                                                            <em><?php _e('Download', 'klapek23_framework'); ?></em>
                                                                        </a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </section>
            <?php endwhile; ?>
        </div>
    </section>
    <?php endif; ?>

    <?php if(!isset( $_COOKIE['disclaimer_accept'])): ?>
    <div class="disclaimer">
        <div class="table">
            <div class="cell">
                <div class="container">
                    <div class="col-md-12">
                        <section class="disclaimer-inside nano">
                            <article class="nano-content">
                                <h2><?php the_field('disclaimer_title'); ?></h2>
                                <section><?php the_field('disclaimer_content'); ?></section>

                                <div class="popup-footer">
                                    <h3><?php _e('Acceptance of Terms:', 'klapek23_framework'); ?></h3>
                                    <div class="popup-nav">
                                        <a href="#" class="button accept-button" title="<?php _e('Accept', 'klapek23_framework'); ?>"><?php _e('Accept', 'klapek23_framework'); ?></a>
                                        <a href="/" class="button" title="<?php _e('Return to home page', 'klapek23_framework'); ?>"><?php _e('Return to home page', 'klapek23_framework'); ?></a>
                                        <a href="#" class="button" title="<?php _e('Return to previous page', 'klapek23_framework'); ?>" onclick="window.history.back(); return false;"><?php _e('Return to previous page', 'klapek23_framework'); ?></a>
                                    </div>
                                    <p><strong><?php the_field('disclaimer_accept_info'); ?></strong></p>
                                </div>
                            </article>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function() {
            var $disclaimer = jQuery('.disclaimer'),
                disclaimerPopup = new popup();

            disclaimerPopup.enablePopup($disclaimer, jQuery('.accept-button'));
            disclaimerPopup.showPopup();



            jQuery(disclaimerPopup).on('hide-popup-stop', function() {
                jQuery.cookie('disclaimer_accept', 1);
            });
        });
    </script>
    <?php endif; ?>

<?php get_footer(); ?>
