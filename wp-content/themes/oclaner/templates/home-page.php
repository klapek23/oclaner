<?php
/*----------
Template name: Home page
---------- */
?>

<?php get_header(); ?>

    <?php if( have_rows('slides') ): ?>
        <div class="main-slider">
            <div class="slides slider-content">

            <?php while( have_rows('slides') ): the_row();

                $subtitle = get_sub_field('subtitle');
                $title = get_sub_field('title');
                $text = get_sub_field('text');
                $link = get_sub_field('link');
                $image = get_sub_field('image');
            ?>

            <div class="slide" style="background-image: url(<?php echo $image; ?>)"; >
                <div class="content">
                    <article class="rounded">
                        <h2><?php echo $title; ?></h2>
                        <h3><?php echo $subtitle; ?></h3>
                        <p><?php echo $text; ?></p>
                        <a href="<?php echo $link; ?>" class="" title="<?php _e('View more', 'klapek23_framework'); ?>"><?php _e('View', 'klapek23_framework'); ?> <i class="icon icon_more_arrow"></i></a>
                    </article>
                </div>
            </div>

            <?php endwhile; ?>

            </div>

            <nav class="pager">
                <ul>
                    <?php while( have_rows('slides') ): the_row(); ?>
                    <li><span><em></em></span></li>
                    <?php endwhile; ?>
                </ul>
            </nav>
        </div>
    <?php endif; ?>

<?php get_footer(); ?>