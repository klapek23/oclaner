// declare a module
var newsApp = angular.module('newsApp', ['ngRoute', 'ngAnimate']);

newsApp.config(function ($routeProvider, $locationProvider) {
    var appRootPath     =   '/wp-content/themes/oclaner/js/angular-modules/news-module/',
        controllersPath =   appRootPath + 'controllers/',
        modelssPath =   appRootPath + 'models/',
        templatesPath =   appRootPath + 'templates/';

    $routeProvider
        // set route for the dynamic page
        .when('/',
        {
            controller: 'NewsListCtrl',
            templateUrl: templatesPath + 'news-list.html'
        })
        .when('/:newsalias',
        {
            controller: 'SingleNewsCtrl',
            templateUrl: templatesPath + 'single-news.html'
        })
        .otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode(true).hashPrefix('!');
});
//data
newsApp.factory('NewsModel', function($http) {

    return {
        getNews: function() {
            return $http.get('/wp-json/wp/v2/news').then( function(response) {
                return response.data;
            });
        },
        getSingleNewsById: function(id) {
            return $http.get('/wp-json/wp/v2/news?filter[id]=' + id).then( function(response) {
                return response.data;
            });
        },
        getSingleNewsByAlias: function(alias) {
            return $http.get('/wp-json/wp/v2/news?filter[name]=' + alias).then( function(response) {
                var data = response.data,
                    news = data[0];

                return news;
            });
        },
        getYears: function() {
            return $http.get('/wp-json/wp/v2/news').then( function(response) {
                var years = [];
                angular.forEach(response.data, function(el, key) {
                    var date = new Date(el.date),
                        year = date.getFullYear();

                    if(years.indexOf(year) == -1) {
                        years.push(year);
                    }
                });

                return years;
            });
        }
    }
});
newsApp.filter('filterByYear', function() {
    return function( items, filterBy) {
        if(filterBy == '' || filterBy == undefined) {
            return items;
        }

        var filtered = [];

        angular.forEach(items, function(item) {
            var date = new Date(item.date),
                year = date.getFullYear();

            if(year == filterBy) {
                filtered.push(item);
            }
        });

        return filtered;
    };
});
//main app controller
newsApp.controller('AppCtrl', ['$scope', 'NewsModel', function($scope, NewsModel, $animate) {
    $scope.appView = 'list';
    $scope.appClass = 'news-content';

    $scope.activeFilters = {
        year    :   ''
    };

    $scope.setAppView = function(view) {
          $scope.appView = view;
    };

    $scope.setAppClass = function(appClass) {
        $scope.appClass = appClass;
    };

    $scope.changeView = function(view, appClass) {
        $scope.setAppView(view);
        $scope.setAppClass(appClass);
    };

    $scope.back = function() {
        window.history.back();
    };

    $scope.setActiveFilters = function(filter, value) {
        $scope.activeFilters[filter] = value;
    }
}]);
newsApp.controller('FiltersCtrl', ['$scope', 'NewsModel', function($scope, NewsModel) {
    NewsModel.getYears().then( function(yearsArray) {
        $scope.years = yearsArray;
    });

    $scope.filterByYear = function(year) {
        $scope.setActiveFilters('year', year);
    };

    $scope.filterByElement = function($event, filter) {
        var value = $($event.currentTarget).text();
        $scope.setActiveFilters(filter, value);
    };
}]);
//main app controller
newsApp.controller('NewsListCtrl', ['$scope', '$sce', 'NewsModel', function($scope, $sce, NewsModel) {
    $scope.setAppView('list');
    $scope.setAppClass('news-content');

    //get all news
    NewsModel.getNews().then( function(newsArray) {
        angular.forEach(newsArray, function(el, i) {
            newsArray[i].acf.intro = $sce.trustAsHtml(newsArray[i].acf.intro);
        });

        $scope.newsList = newsArray;

        $(document).ready( function() {
            setTimeout(function() {
                $('#yearFilter').select2({
                    allowClear: true
                });
            }, 500);
        });
    });
}]);

//main app controller
newsApp.controller('SingleNewsCtrl', ['$scope', '$routeParams', '$sce', 'NewsModel', function($scope, $routeParams, $sce, NewsModel) {
    var alias = $routeParams.newsalias;

    $scope.setAppView('single');
    $scope.setAppClass('single-news-content');

    NewsModel.getSingleNewsByAlias(alias).then( function(newsObj) {
        $scope.news = newsObj;
        $scope.news.content = $sce.trustAsHtml($scope.news.content.rendered);
    });
}]);
newsApp.directive("newsAnimate", ['$animate', function($animate){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            var $spinner = $(elem).siblings('.loader');

            $animate.on('enter', elem, function(element, phase) {

                //check element
                if($(element).hasClass('newsAnimate')) {
                    switch (phase) {
                        case 'start':
                            $spinner.addClass('active');
                            break;
                        case 'close':
                            $spinner.removeClass('active');
                            break;
                    };
                }
            });
        }
    };
}]);
//# sourceMappingURL=news-module.js.map