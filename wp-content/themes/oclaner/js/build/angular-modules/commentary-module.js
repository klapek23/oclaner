// declare a module
var app = angular.module('commentaryApp', ['ngAnimate', 'angular-toArrayFilter']);

app.factory('itemsFactory', function($http, $q) {

    return {
        getItemsByCat: function(cat) {
            return $http.get('/wp-json/wp/v2/wealth-management-items?filter[category_name]=' + cat + '&per_page=99999').then( function(response) {
                angular.forEach(response.data, function(el, key) {
                    var date = new Date(el.date),
                        year = date.getFullYear();

                    response.data[key].year = year;
                });

                return response.data;
            });
        },
        getYearsByCat: function(cat) {
            return $http.get('/wp-json/wp/v2/wealth-management-items?filter[category_name]=' + cat).then( function(response) {
                var years = [];
                angular.forEach(response.data, function(el, key) {
                    var date = new Date(el.date),
                        year = date.getFullYear();

                    if(years.indexOf(year) == -1) {
                        years.push(year);
                    }
                });

                return years;
            });
        },
        updateYearsFilter: function(items) {
            var years = [];
            angular.forEach(items, function(el, key) {
                var date = new Date(el.date),
                    year = date.getFullYear();

                if(years.indexOf(year) == -1) {
                    years.push(year);
                }
            });


            return years;
        },
        getSubcategoriesByCat: function(cat) {
            return $http.get('/wp-json/wp/v2/wealth-management-items?filter[category_name]=' + cat).then( function(response) {
                var subcategories = [];
                angular.forEach(response.data, function(el, key) {
                    var subcategoriesArray = [];
                    angular.forEach(el.subcategory, function(obj, key) {
                        subcategoriesArray.push(obj.name);
                    });

                    angular.forEach(subcategoriesArray, function(subcategory, key) {
                        if(subcategories.indexOf(subcategory) == -1) {
                            subcategories.push(subcategory);
                        }
                    });
                });

                return subcategories;
            });
        },
        updateSubcategoriesFilter: function(items) {
            var subcategories = [];
            angular.forEach(items, function(el, key) {
                if(subcategories.indexOf(el.subcategory) == -1) {
                    subcategories.push(el.subcategory);
                }
            });

            return subcategories;
        }
    }
});

app.filter('filterByYear', function() {
    return function( items, filterBy) {
        if(filterBy == '' || filterBy == undefined) {
            return items;
        }

        var filtered = [];

        angular.forEach(items, function(item) {
            var date = new Date(item.date),
                year = date.getFullYear();

            if(year == filterBy) {
                filtered.push(item);
            }
        });

        return filtered;
    };
});


//main app controller
app.controller('appCtrl', ['$scope', 'itemsFactory', function($scope, itemsFactory) {
    $scope.categories = [
        {
            name: 'Monthly Commentary',
            id: 'monthly-commentary'
        },
        {
            name: 'Factsheet',
            id: 'factsheet'
        },
        {
            name: 'KIID',
            id: 'kiid'
        },
        {
            name: 'Prospectus',
            id: 'prospectus'
        }
    ];
    $scope.activeCategoryId = 'monthly-commentary';

    $scope.items = [];
    $scope.filters = {
        years: {
            name: 'years',
            title: 'Years',
            items: []
        },
        subcategories: {
            name: 'subcategories',
            title: 'Funds',
            items: []
        }
    };

    $scope.activeFilters = {
        years: '',
        subcategories: ''
    };

    $scope.animate = true;

    //setters
    $scope.setActiveCategoryId = function(id) {
        $scope.activeCategoryId = id;
    };
    $scope.setItems = function(newItems) {
        $scope.items = newItems;
    };

    $scope.setFilterYears = function(newYears) {
        $scope.filters.years.items = newYears;
    };

    $scope.setFiltersSubcategories = function(subcategories) {
        $scope.filters.subcategories.items = subcategories;
    };

    $scope.setActiveFilter = function(filter, value) {
        $scope.activeFilters[filter] = value;
    };

    $scope.setAnimateValue = function(value) {
        $scope.animate = value;
    };

    $scope.setAnimateValue(true);


    //get first category items
    itemsFactory.getItemsByCat($scope.categories[0].id).then( function(items) {
        $scope.items = items;

        var years = itemsFactory.updateYearsFilter(items);
        $scope.setFilterYears(years);

        var subcategories = itemsFactory.updateSubcategoriesFilter(items);
        $scope.setFiltersSubcategories(subcategories);

        $scope.setAnimateValue(false);
    });
}]);



app.controller('filtersCtrl', ['$scope', function($scope) {
    $scope.filterByYear = function(year) {
        $scope.setActiveFilter('year', year);
    };

    $scope.filterByElement = function($event, filter) {
        var value = $($event.currentTarget).text();
        $scope.setActiveFilter(filter, value);
    };

    $scope.yearOrCategory = function(item) {
        return card.values.opt1 + card.values.opt2;
    };

    $scope.sortOrder = function(type) {
        if(isNaN(type)) {
            return '-item';
        } else {
            return '+item';
        }
    };
}]);


app.controller('navCtrl', ['$scope', 'itemsFactory', function($scope, itemsFactory) {

    $scope.changeOnClick = function($event) {
        var cat = $($event.currentTarget).data('name');

        $scope.setAnimateValue(true);

        $scope.setActiveCategoryId(cat);

        itemsFactory.getItemsByCat(cat).then( function(items) {
            $scope.setItems(items);

            var years = itemsFactory.updateYearsFilter($scope.items);
            $scope.setFilterYears(years);

            var subcategories = itemsFactory.updateSubcategoriesFilter($scope.items);
            $scope.setFiltersSubcategories(subcategories);

            $scope.setActiveFilter('years', '');
            $scope.setActiveFilter('subcategories', '');

            setTimeout(function() {
                $('.commentary-section').closest('.tabs-content').height($('.commentary-section').closest('.slide.active').height());
            }, 500);

            $scope.setAnimateValue(false);
        });
    };
}]);


app.controller('listCtrl', ['$scope', '$http', function($scope, $http) {

}]);
//# sourceMappingURL=commentary-module.js.map