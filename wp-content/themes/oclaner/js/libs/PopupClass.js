var PopupClass = function() {
    this.$el;
    this.$content;
    this.$closeButton;
};

PopupClass.prototype = {
    createPopup: function() {
        $(this).trigger('create-popup');
    },
    enablePopup: function($el, $closeButton) {
        this.$el = $el;
        this.$content = this.$el.closestChild('section');
        this.$closeButton = this.$el.find($closeButton);

        this.$el.addClass('oclaner-popup');
        this.$content.addClass('oclaner-popup-content');

        this.$closeButton.on('click', function() {
            this.hidePopup();
            return false;
        }.bind(this));

        $(this).trigger('enable-popup');
    },
    showPopup: function() {
        $('body').css('overflow', 'hidden');

        $(this).trigger('show-popup-start');

        this.$el.fadeIn( function() {

            this.$content.nanoScroller();

            $(this).trigger('show-popup-stop');
        }.bind(this));
    },
    hidePopup: function() {
        $('body').css('overflow', 'auto');

        $(this).trigger('hide-popup-start');

        this.$el.fadeOut( function() {
            $(this).trigger('hide-popup-stop');
        }.bind(this));
    }
};

window.popup = PopupClass;