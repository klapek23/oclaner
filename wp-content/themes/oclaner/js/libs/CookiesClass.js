var CookiesClass = function() {
    this.$el;
    this.$content;
    this.$closeButton;
};

CookiesClass.prototype = {
    createPopup: function() {
        $(this).trigger('create-popup');
    },
    enablePopup: function($el, $closeButton) {
        this.$el = $el;
        this.$content = this.$el.closestChild('section');
        this.$closeButton = this.$el.find($closeButton);

        this.$closeButton.on('click', function() {
            this.hidePopup();
            return false;
        }.bind(this));

        $(this).trigger('enable-popup');
    },
    showPopup: function() {
        $(this).trigger('show-popup-start');

        this.$el.fadeIn( function() {
            $(this).trigger('show-popup-stop');
        }.bind(this));
    },
    hidePopup: function() {
        $(this).trigger('hide-popup-start');

        this.$el.fadeOut( function() {
            $(this).trigger('hide-popup-stop');
        }.bind(this));
    }
};

window.cookies = CookiesClass;