var animateNavClass = function(el) {
    this.$el = $(el);
    this.items = this.getItems();
    this.$border = $('<div class="border-bottom" style="height: 1px; background-color: #000"></div>');
    this.$active = '';
};

animateNavClass.prototype = {
    constructor: animateNavClass,
    init: function() {
        this.createBorder();

        this.$el.addClass('animated-nav');

        this.$active = this.getActiveItem();

        this.$el.find('li').on('click', function(e) {
            var $target = $(e.currentTarget);
            this.changeActive($target);
        }.bind(this));

        $(window).resize(function() {
            this.refreshPosition();
        }.bind(this));
    },
    changeActive: function($newitem) {
        $.each(this.items, function(i, el) {
            $(el).removeClass('animate-nav-active');
        });

        var left    = $newitem.position().left,
            width   = $newitem.outerWidth();

        this.animateBorder(left, width);
        $newitem.addClass('animate-nav-active');
    },
    createBorder: function() {
        var left    = this.items[0].position().left,
            width   = this.items[0].outerWidth();

        this.items[0].addClass('animate-nav-active');

        this.$border.appendTo(this.$el);
        this.$border.css({
            'transform'             :   'translateX('+ (left - 15) +'px)',
            '-webkit-transform'     :   'translateX('+ (left - 15) +'px)',
            '-moz-transform'        :   'translateX('+ (left - 15) +'px)',
            width                   :   width
        });
    },
    getItems: function() {
        var itemsArray = [];
        $.each(this.$el.find('li'), function(i,el) {
            itemsArray.push($(el));
        });

        return itemsArray;
    },
    animateBorder: function(left, width) {
        this.$border.css({
            'transform'             :   'translateX('+ (left - 15) +'px)',
            '-webkit-transform'     :   'translateX('+ (left - 15) +'px)',
            '-moz-transform'        :   'translateX('+ (left - 15) +'px)',
            width                   :   width
        }).addClass('active');
    },
    refreshPosition: function() {
        this.$active = this.getActiveItem();

            var left = this.$active.position().left,
                width = this.$active.outerWidth();

            this.animateBorder(left, width);
    },
    getActiveItem: function() {
        return this.$el.find('li.animate-nav-active');
    },
    hideBorder: function() {
        this.$border.css({opacity: 0}).addClass('hidden');
    },
    showBorder: function() {
        this.$border.css({opacity: 1}).removeClass('hidden');
    },
    getInstance: function() {
        return this;
    }
};