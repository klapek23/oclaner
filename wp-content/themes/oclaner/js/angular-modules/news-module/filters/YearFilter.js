newsApp.filter('filterByYear', function() {
    return function( items, filterBy) {
        if(filterBy == '' || filterBy == undefined) {
            return items;
        }

        var filtered = [];

        angular.forEach(items, function(item) {
            var date = new Date(item.date),
                year = date.getFullYear();

            if(year == filterBy) {
                filtered.push(item);
            }
        });

        return filtered;
    };
});