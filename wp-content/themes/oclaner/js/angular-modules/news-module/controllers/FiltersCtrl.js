newsApp.controller('FiltersCtrl', ['$scope', 'NewsModel', function($scope, NewsModel) {
    NewsModel.getYears().then( function(yearsArray) {
        $scope.years = yearsArray;
    });

    $scope.filterByYear = function(year) {
        $scope.setActiveFilters('year', year);
    };

    $scope.filterByElement = function($event, filter) {
        var value = $($event.currentTarget).text();
        $scope.setActiveFilters(filter, value);
    };
}]);