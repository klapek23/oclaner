//main app controller
newsApp.controller('NewsListCtrl', ['$scope', '$sce', 'NewsModel', function($scope, $sce, NewsModel) {
    $scope.setAppView('list');
    $scope.setAppClass('news-content');

    //get all news
    NewsModel.getNews().then( function(newsArray) {
        angular.forEach(newsArray, function(el, i) {
            newsArray[i].acf.intro = $sce.trustAsHtml(newsArray[i].acf.intro);
        });

        $scope.newsList = newsArray;

        $(document).ready( function() {
            setTimeout(function() {
                $('#yearFilter').select2({
                    allowClear: true
                });
            }, 500);
        });
    });
}]);
