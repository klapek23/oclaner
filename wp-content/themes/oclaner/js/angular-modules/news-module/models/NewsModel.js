//data
newsApp.factory('NewsModel', function($http) {

    return {
        getNews: function() {
            return $http.get('/wp-json/wp/v2/news').then( function(response) {
                return response.data;
            });
        },
        getSingleNewsById: function(id) {
            return $http.get('/wp-json/wp/v2/news?filter[id]=' + id).then( function(response) {
                return response.data;
            });
        },
        getSingleNewsByAlias: function(alias) {
            return $http.get('/wp-json/wp/v2/news?filter[name]=' + alias).then( function(response) {
                var data = response.data,
                    news = data[0];

                return news;
            });
        },
        getYears: function() {
            return $http.get('/wp-json/wp/v2/news').then( function(response) {
                var years = [];
                angular.forEach(response.data, function(el, key) {
                    var date = new Date(el.date),
                        year = date.getFullYear();

                    if(years.indexOf(year) == -1) {
                        years.push(year);
                    }
                });

                return years;
            });
        }
    }
});