<?php
//theme settings page class

class ThemeSettingsPageClass {

    //add page to menu
    public function addPageToMenu() {
        add_submenu_page('themes.php', 'Theme settings page', 'Theme settings', 'manage_options', 'theme-settings', array($this, 'theme_settings'));
    }

    //print page
    public function theme_settings() {
        $this->theme_settings_scripts();
        $this->theme_settings_styles();
        $this->print_content();
    }

    //update fields in database
    private function themeoptions_update() {
        /*update_option('main-logo-image', $_POST['main-logo-image']);*/
        update_option('cookies-link', $_POST['cookies-link']);
        update_option('ga-code', $_POST['ga-code']);
    }

    //add scripts to media uploader
    private function theme_settings_scripts() {
        wp_enqueue_script('jquery');
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_register_script('my-upload', get_template_directory_uri() . '/js/libs/my-upload.js', array('jquery','media-upload','thickbox'));
        wp_enqueue_script('my-upload');
    }

    private function theme_settings_styles() {
        wp_enqueue_style('thickbox');
        wp_register_style('custom-admin', get_template_directory_uri() . '/css/libs/custom-admin.css');
        wp_enqueue_style('custom-admin');
    }

    //print content
    private function print_content() { ?>
        <?php if ( $_POST['update_options'] == 'true' ) { $this->themeoptions_update(); } ?>
        <div class="wrap">
            <div id="icon-themes" class="icon32"><br /></div>
            <h2>Theme settings</h2>

            <form method="POST" action="">
                <ul>
					<!--<li class="optionContainer">
                        <h3>Main logo:</h3>
                        <div>
                            <label for="footer-fb-icon">Main logo image</label>
                            <img src="<?php /*echo get_option('main-logo-image'); */?>" title="Main logo" style="display: block; margin-bottom: 10px;">
                            <input type="text" name="main-logo-image" id="main-logo-image" value="<?php /*echo get_option('main-logo-image'); */?>" >
                            <button type="button" class="upload-button"  name="main-logo-image" id="main-logo-image" >Choose</button>
                        </div>
                    </li>-->
                    <li class="optionContainer">
                        <h3>Cookies bubble:</h3>
                        <div>
                            <label for="cookies-link">Cookies link url</label>
                            <input type="text" name="cookies-link" id="cookies-link" value="<?php echo get_option('cookies-link'); ?>" >
                        </div>
                    </li>
                    <li class="optionContainer">
                        <h3>Google Analytics code:</h3>
                        <div>
                            <label for="ga-code" style="float: left;">GA code</label>
                            <textarea name="ga-code" id="ga-code" rows="10" cols="60" ><?php echo stripslashes(get_option('ga-code')); ?></textarea>
                        </div>
                    </li>
                    <li>
                        <input type="hidden" name="update_options" value="true" />
                        <button type="submit" class="button" />Save</button>
                    </li>
                </ul>
            </form>
        </div>
    <?php 
    }
}

?>