<?php
//register widgetized area

class RegisterWidgetizedAreaClass {

    public function widgets_init() {
        register_sidebar( array(
            'name'          => __( 'Sidebar menu bottom', 'klapek23_framework' ),
            'id'            => 'sidebar-menu-bottom',
            'before_widget' => '<article>',
            'after_widget'  => '</article>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>'
        ) );

        register_sidebar( array(
            'name'          => __( 'Bottom site info', 'klapek23_framework' ),
            'id'            => 'bottom-site-info',
            'before_widget' => '',
            'after_widget'  => '',
            'before_title'  => '',
            'after_title'   => ''
        ) );
    }
    
}

?>