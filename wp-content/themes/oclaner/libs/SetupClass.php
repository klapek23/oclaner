<?php
//remove admin pages

class SetupClass {

    //make redirections available
    public function do_output_buffer() {
        ob_start();
    }

    //load plugin textdomain (available for translations)
    public function loadTextdomain() {
        load_theme_textdomain( 'klapek23_framework', get_template_directory() . '/languages' );
    }

    //register menus
    public function registerMenus() {
        register_nav_menus( array(
            'primary' => __( 'Main menu', 'main-menu' ),
        ));
    }

    //add post formats support
    public function addPostFormat() {
        add_theme_support( 'post-formats', array('aside'));
    }

    //add HTML5 support for search form
    public function addHTML5() {
        add_theme_support( 'html5', array( 'search-form') );
    }
        
    //add support for do shortcodes in text widget
    public function widgetDoShortcodes() {
        add_filter('widget_text', 'do_shortcode');
    }

    public function addImagesSizes() {
        add_image_size( 'post-thumbnail', '150', '100', true);
    }

    public function addThumbsSupport() {
        add_theme_support( 'post-thumbnails' );
    }

    public function disableAdminBar() {
        show_admin_bar(false);
    }

    //enqueue additional scripts and styles
    function oclaner_scripts() {
        wp_deregister_script( 'jquery' );

        wp_register_style('style', get_template_directory_uri() . (getenv('APPLICATION_ENV') == 'dev' ? '/css/build/dev/style.css' : '/css/build/prod/style.css') . '"' );
        //wp_register_style('circles', get_template_directory_uri() . '/circles.css');
        wp_register_script('scripts', get_template_directory_uri() . (getenv('APPLICATION_ENV') == 'dev' ? '/js/build/dev/scripts.js' : '/js/build/prod/scripts.js') . '"');

        wp_enqueue_style('style');
        wp_enqueue_style('circles');
        wp_enqueue_script('scripts');
    }

    //render widgets titles as html
    public function html_widget_title( $title ) {
        $title = str_replace( '[', '<', $title );
        $title = str_replace( '[/', '</', $title );

        $title = str_replace( 's]', 'strong>', $title );
        $title = str_replace( 'e]', 'em>', $title );
        $title = str_replace('br]', 'br>', $title);

        return $title;
    }

}

?>