<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Gardimax
 */

get_header(); ?>

<div class="main-banner" style="background-image: url(<?php echo get_template_directory_uri() . '/files/banner_about_us.jpg'; ?>);">
    <div class="content">
        <article class="rounded">
            <h3><?php _e('ERROR' ,'klapek23_framework'); ?></h3>
            <h2><?php _e('404' ,'klapek23_framework'); ?></h2>
            <p><?php _e("Page dosen't exist" ,'klapek23_framework'); ?></p>
        </article>
    </div>
</div>

<?php get_footer(); ?>