<?php
/* ----------
Template name: Archive news
------------ */
?>

<?php get_header(); ?>

<?php
$newspage = get_page_by_path('news');

$mainBanner = get_field('main_banner_image', $newspage->ID);
$mainBannerImage = $mainBanner['url'];
?>

    <div class="main-banner slide" data-width="1920" data-height="1200" style="background-image: url(<?php echo $mainBannerImage; ?>);">
        <div class="content">
            <article class="rounded">
                <h3><?php the_field('main_banner_subtitle', $newspage->ID) ?></h3>
                <h2><?php the_field('main_banner_title', $newspage->ID) ?></h2>
                <?php /*if(get_field('main_banner_text')): */?><!--
                    <p><?php /*the_field('main_banner_text', $newspage->ID); */?></p>
                --><?php /*endif; */?>
            </article>
        </div>
    </div>

    <section ng-app="newsApp" ng-controller="AppCtrl" ng-class="['content-section', appClass]">
        <div class="container">
            <div class="row">
                <div ng-class="{ 'col-md-8' : appView == 'list', 'col-md-12' : appView == 'single' }">
                    <h2><?php _e('News / Press / Events', 'klapek23_framework'); ?></h2>
                </div>
                <div class="col-md-4 news-years-filter" ng-if="appView == 'list'">
                    <nav class="filter" ng-controller="FiltersCtrl">
                        <span class="title"><?php _e('Search by year:', 'klapek23_framework'); ?></span>

                        <select name="yearFilter" id="yearFilter" data-placeholder="Choose..." ng-model="activeFilters.year">
                            <option value=""> - </option>
                            <option ng-repeat="year in years" value="{{year}}">{{year}}</option>
                        </select>

                       <!-- <ul>
                            <li ng-click="filterByYear('');"><?php /*_e('All', 'klapek23_framework'); */?></li>
                            <li ng-repeat="year in years" ng-click="filterByElement($event, 'year');">{{ year }}</li>
                        </ul>-->
                    </nav>
                </div>
            </div>
            <div class="row news-back-button" ng-if="appView == 'single'">
                <div class="col-md-12">
                    <div class="news-top-nav">
                        <a href="#" ng-click="back();">
                            <i class="icon icon_arrow-left"></i> <?php _e('Back', 'klapek23_framework'); ?>
                        </a>
                    </div>
                </div>
            </div>
            <div class="newsContainer">
                <div class="loader"><div class="table"><div class="cell"><i class="icon icon_spinner-icon"></i></div></div></div>
                <div class="newsAnimate" ng-view news-animate></div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>