<?php
/**
 * OperatorSystems functions and definitions
 *
 */


/* ---------- require classes ------ */
//setup class
require_once('libs/SetupClass.php');

//remove admin pages class
require_once('libs/RemoveAdminPagesClass.php');

//register widgetized area class
require_once('libs/RegisterWidgetizedAreaClass.php');

//theme settings page class
require_once('libs/ThemeSettingsPageClass.php');

//mime type class
require_once('libs/MimeTypeClass.php');

//cross domain cookie class
require_once('libs/CrossDomainCookiesClass.php');



/* ---------- create objects and add actions ----- */
//operator setup
$setup = new SetupClass();
add_action('init', array($setup, 'do_output_buffer'));
add_action('wp_enqueue_scripts', array($setup, 'oclaner_scripts'));
add_filter( 'widget_title', array($setup, 'html_widget_title'));
$setup->loadTextdomain();
$setup->registerMenus();
$setup->addPostFormat();
$setup->addHTML5();
$setup->widgetDoShortcodes();
$setup->addImagesSizes();
$setup->disableAdminBar();
$setup->addThumbsSupport();


//register widgetized area for WP
$registerWidgets = new RegisterWidgetizedAreaClass();
add_action( 'widgets_init', array($registerWidgets, 'widgets_init'));


//remove admin pages action
$removeAdminPages = new RemoveAdminPagesClass();
add_action("admin_menu", array($removeAdminPages, "removePostsPage"));


// add action to display theme options page
$addThemeSettignsPage = new ThemeSettingsPageClass();
add_action("admin_menu", array($addThemeSettignsPage, "addPageToMenu"));

//add cross domain cookies
$crossDomainCookies = new crossDomainCookiesClass();
add_action("init", array($crossDomainCookies, "init"));

// add action to display theme options page
$mimeTypes = new MimeTypeClass();
add_filter('upload_mimes', array($mimeTypes, 'custom_upload_mimes'));

add_filter('json_api_encode', 'json_api_encode_acf');

?>