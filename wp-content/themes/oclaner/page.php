<?php
/* ----------
Template name: About Us
------------ */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <section class="content-section standard-content default-page">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?php the_title(); ?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><?php the_content(); ?></div>
                </div>
            </div>
        </article>
    </section>
<?php endwhile; endif; ?>

<?php get_footer(); ?>